# From: r.meier@siemens.com
#use wml::debian::translation-check translation="7dddaeaa5c20d8b39e4a465987ff2f4526a84517"
# Translation: Luis Muñoz Fuente luis.munoz.edu@juntadeandalucia.es 2022-04-14
# Webpage URL is usually redirected, this is normal, Roger Meier 2005-09-20

<define-tag pagetitle>Siemens</define-tag>
<define-tag webpage>https://www.siemens.com/</define-tag>

#use wml::debian::users

<p>
  Siemens usa Debian en varias áreas, incluyendo I+D, infraestructura, productos 
  y soluciones.
  </p>
<p>    
  Debian se utiliza como la base de muchos de nuestros productos y servicios en 
  automatización y digitalización, de procesos e industrias de fabricación, 
  soluciones de movilidad inteligente para carreteras y ferrocarriles, servicios 
  digitales de salud, tecnología médica así como sistemas de energía 
  distribuida e infraestructura inteligente para edificios.
</p>
<p>
  Debian está, por ejemplo, incrustrado y forma parte de los
  <a href="https://www.siemens-healthineers.com/es/magnetic-resonance-imaging">Escáneres IRM Siemens</a>,
  a bordo de los modernos sistemas de tren y metro Siemens, alrededor de las  
  <a href="https://press.siemens.com/global/en/feature/siemens-digitalize-norwegian-railway-network">vías y estaciones de tren</a>,
  y muchos más de nuestros dispositivos. Además, Siemens es miembro fundador de la  
  <a href="https://www.cip-project.org/">Plataforma de Infraestructura Civil</a>,
  y usa Debian LTS como capa base.
</p>
<p>    
  Por supuesto, Debian también es parte esencial de la infraestructura de nuestro
  servidor. A menudo usamos Debian en nuestros ordenadores de sobremesa
  si se requiere un sistema Linux, la mayoría de los productos de tecnología de
  construcción basados ​​en Linux están desarrollados en Debian.
  Siemens también ofrece distribuciones integradas basadas en Debian, como
  <a href="https://siemens.com/embedded">Sokol™ Omni OS</a>.
</p>
<p>    
  Por qué elegimos Debian: la alta calidad y estabilidad que obtenemos gracias 
  a Debian es genial y la disponibilidad de actualizaciones de software y parches
  de seguridad es único, y la cantidad de paquetes de software disponibles es increíble.
</p>

