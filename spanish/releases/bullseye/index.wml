#use wml::debian::template title="Información sobre Debian &ldquo;bullseye&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="4c728c46ce7c43bebb50236f2439e9bc75f33a36"

<p>Debian <current_release_bullseye> se
publicó el <a href="$(HOME)/News/<current_release_newsurl_bullseye/>"><current_release_date_bullseye></a>.
<ifneq "11.0" "<current_release>"
  "Debian 11.0 se publicó inicialmente el <:=spokendate('2021-08-14'):>."
/>
Esta versión incluye muchos cambios
importantes, que se describen en
nuestra <a href="$(HOME)/News/2021/20210814">nota de prensa</a> y
en las <a href="releasenotes">notas de publicación</a>.</p>

<p><strong>Debian 11 ha sido reemplazada por
<a href="../bookworm/">Debian 12 (<q>bookworm</q>)</a>.
#Las actualizaciones de seguridad han dejado de proporcionarse el <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>Sin embargo, bullseye se beneficia del soporte a largo plazo (LTS, por sus siglas en inglés) hasta
#finales de xxxxx de 20xx. El LTS está limitado a i386, amd64, armel, armhf y arm64.
#El resto de arquitecturas ya no están soportadas en bullseye.
#Para más información, consulte la <a
#href="https://wiki.debian.org/LTS">sección LTS de la wiki de Debian</a>.
#</strong></p>

<p>Para obtener e instalar Debian, consulte
la página con <a href="debian-installer/">información para la instalación</a> y la
<a href="installmanual">guía de instalación</a>. Para actualizar desde una versión
anterior de Debian, consulte las instrucciones incluidas en las
<a href="releasenotes">notas de publicación</a>.</p>

### Activate the following when LTS period starts.
#<p>Arquitecturas soportadas durante el periodo de soporte a largo plazo:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Arquitecturas soportadas cuando se publicó inicialmente bullseye:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>En contra de nuestros deseos, puede haber algunos problemas en esta
versión, a pesar de haber sido declarada <em>estable</em>. Hemos hecho
<a href="errata">una lista de los principales problemas conocidos</a>, y siempre puede
<a href="reportingbugs">informarnos de otros</a>.</p>

<p>Por último, pero no menos importante, tenemos una lista de las <a href="credits">personas que han
contribuido</a> a hacer posible esta publicación.</p>
