#use wml::debian::translation-check translation="9bf43d9ebe1fe8f0f648cd5c0431b530d1105d92" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i HTTP-servern Apache.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17189">CVE-2018-17189</a>

	<p>Gal Goldshtein från F5 Networks upptäckte en överbelastningssårbarhet
	i mod_http2. Genom att skicka felaktigt formatterade förfrågningar upptar
	den förfrågans http/2-ström onödvändigt en servertråd och rensar upp
	inkommnade data, vilket resulterar i överbelastning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17199">CVE-2018-17199</a>

	<p>Diego Angulo från ImExHS upptäckte att mod_session_cookie inte
	respekterar utgångstid.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0196">CVE-2019-0196</a>

	<p>Craig Young upptäckte att http/2-förfrågehanteringen i mod_http2
	kunde göras att den får åtkomst till friat minne i stringjämförelser när
	den avgör en förfrågans metod och därmed behandlar förfrågan felaktigt.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0211">CVE-2019-0211</a>

	<p>Charles Fol upptäckte en utökning av privilegier från
	ickepriviligierade barnprocesser till föräldraprocessen som kör som root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0217">CVE-2019-0217</a>

	<p>En kapplöpningseffek i mod_auth_digest vid körning i en trådad
	server kunde tillåta en användare med giltiga legitimationsuppgifter
	att autentisera med ett annat användarnamn, och därmed förbigå
	konfigurerade åtkomstkontrollsrestriktioner. Problemet upptäcktes av
	Simon Kappel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0220">CVE-2019-0220</a>

	<p>Bernhard Lorenz från Alpha Strike Labs GmbH rapporterade att
	URL-normaliseringar hanterades inkonsekvent. När sökvägskomponenten
	i en förfråge-URL innehåller flera på varandra följande snedstreck ('/'),
	måste direktiv som LocationMatch och RewriteRule stå för dubbletter
	i reguljära uttryck medan andra aspekter av de behandlande servrarna kommer
	att kollapsa dem implicit.</p></li>

</ul>

<p>För den stabila utgåvan (Stretch) har dessa problem rättats i
version 2.4.25-3+deb9u7.</p>

<p>Denna uppdatering innehåller även felrättningar som var schemalagda för
inkludering i nästa stabila punktutgåva. Detta inkluderar en rättning för en
regression som orsakades av en säkerhetsrättning i 2.4.25-3+deb9u6.</p>

<p>Vi rekommenderar att ni uppgraderar era apache2-paket.</p>

<p>För detaljerad säkerhetsstatus om apache2 vänligen se
dess säkerhetsspårare på <a href="https://security-tracker.debian.org/tracker/apache2">https://security-tracker.debian.org/tracker/apache2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4422.data"
