# Status: [open-for-edit]
# $Rev$
#use wml::debian::translation-check translation="9c0ebe940eaf29e78367427aea8e02f46fb70bcd" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Langzeitunterstützung für Debian 8 läuft aus</define-tag>
<define-tag release_date>2020-07-09</define-tag>
#use wml::debian::news

##
## Translators:
## - if translating while the file is in publicity-team/announcements repo,
##   please ignore the translation-check header. Publicity team will add it
##   including the correct translation-check header when moving the file
##   to the web repo
##
## - if translating while the file is in webmaster-team/webwml repo,
##   please use the copypage.pl script to create the page in your language
##   subtree including the correct translation-check header.
##

<p>Das Debian-LTS-Team gibt bekannt, dass die Betreuung und Wartung von 
Debian 8 <q>Jessie</q> am 30. Juni 2020 eingestellt wurde. Das sind fünf Jahre 
nach seiner ursprünglichen Veröffentlichung am 26. April 2015.</p>

<p>Debian wird keine weiteren Sicherheitsaktualisierungen für Debian 8 
anbieten. Für einige <q>Jessie</q>-Pakete bestehen jedoch Ausnahmen; diese 
werden durch Dritte weiter unterstützt. Detaillierte Informationen finden 
sich unter <a href="https://wiki.debian.org/LTS/Extended">
Extended LTS</a>.</p>

<p>
Das LTS-Team wird jetzt den Übergang zu Debian 9 <q>Stretch</q> vorbereiten, 
welches sich zur Zeit in Oldstable befindet, und hat zum 6. Juli 2020 bereits 
die Aufgaben des Sicherheits-Teams übernommen. Die finale 
Zwischenveröffentlichung für <q>Stretch</q> wird am 18. Juli 2020 
herausgegeben.
</p>

<p>Debian 9 wird ebenfalls für fünf Jahre nach seiner ursprünglichen 
Veröffentlichung Langzeitunterstützung (Long Term Support) erhalten, d. h. 
bis zum 30. Juni 2022. Sie umfasst die Architekturen amd64, i386, 
armel und armhf sowie, wie wir erfreut bekanntgeben dürfen, zum ersten Mal 
auch arm64.
</p>

<p>Weitere Informationen über den Einsatz von <q>Stretch</q> LTS und 
Upgrades von <q>Jessie</q> finden Sie unter  
<a href="https://wiki.debian.org/LTS/Using">LTS/Using</a>.</p>

<p>Debian und sein LTS-Team möchten allen beitragenden Anwenderinnen und 
Anwendern, Entwicklerinnen und Entwicklern sowie den Sponsoren dafür danken, 
dass sie es möglich machen, alte Stable-Versionen am Leben zu erhalten. 
Ihretwegen ist diese Langzeitunterstützung so erfolgreich.</p>

<p>Falls Sie sich auf Debian LTS verlassen, erwägen Sie bitte, 
<a href="https://wiki.debian.org/LTS/Development">dem Team beizutreten</a>,
Korrekturen einzureichen, Tests durchzuführen oder 
<a href="https://wiki.debian.org/LTS/Funding">finanzielle Unterstützung zu leisten</a>.</p>

<h2>Über Debian</h2>

<p>Das Debian-Projekt wurde 1993 von Ian Murdock als wirklich freies 
Gemeinschaftsprojekt gegründet. Seitdem ist das Projekt zu einem der größten 
und einflussreichsten Open-Source-Projekte angewachsen. Tausende von 
Freiwilligen aus aller Welt arbeiten zusammen, um Debian-Software herzustellen 
und zu betreuen. Verfügbar in über 70 Sprachen und eine große Bandbreite an 
Rechnertypen unterstützend bezeichnet sich Debian als das <q>universelle 
Betriebssystem</q>.</p>

<h2>Weitere Informationen</h2>
<p>Weitere Informationen über die Debian-Langzeitunterstützung finden Sie unter 
<a href="https://wiki.debian.org/LTS/">https://wiki.debian.org/LTS/</a>.</p>


<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter 
<a href="$(HOME)/">https://www.debian.org/</a> oder schicken eine E-Mail (in 
Englisch) an 
&lt;press@debian.org&gt;.</p>
