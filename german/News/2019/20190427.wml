<define-tag pagetitle>Debian 9 aktualisiert: 9.9 veröffentlicht</define-tag>
<define-tag release_date>2019-04-27</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="f797e8b952b201d953c4e7de221fd635808136e4" maintainer="Erik Pfannenstein"

<define-tag release>9</define-tag>
<define-tag codename>Stretch</define-tag>
<define-tag revision>9.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die neunte Aktualisierung seiner 
Stable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung behebt hauptsächlich 
Sicherheitslücken der Stable-Veröffentlichung sowie einige ernste Probleme. 
Für sie sind bereits separate Sicherheitsankündigungen veröffentlicht worden, auf 
die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Aktualisierung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete 
auffrischt. Es gibt keinen Grund, <codename>-Medien zu entsorgen, da deren 
Pakete nach der Installation mit Hilfe eines aktuellen Debian-Spiegelservers 
auf den neuesten Stand gebracht werden können. 
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird 
nicht viele Pakete auf den neuesten Stand bringen müssen. Die meisten dieser 
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden. 
</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian 
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter: 
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<p>
Als eine Besonderheit bei dieser Revision müssen diejenigen, die zum 
Aktualisieren <q>apt-get</q> verwenden, dieses Mal unbedingt den 
<q>dist-upgrade</q>-Befehl benutzen, damit die neuesten Kernel-Pakete 
eingespielt werden. Wer etwas Anderes wie beispielsweise <q>apt</q> oder 
<q>aptitude</q> einsetzt, sollte den <q>upgrade</q>-Befehl verwenden.
</p>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Stable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction audiofile "Probleme mit Dienstblockaden [CVE-2018-13440] und Pufferüberläufen gelöst [CVE-2018-17095]">
<correction base-files "Auf die Zwischenveröffentlichung aktualisiert">
<correction bwa "Pufferüberlauf behoben [CVE-2019-10269]">
<correction ca-certificates-java "bashisms in postinst und jks-keystore überarbeitet">
<correction cernlib "Bei Fortran-Modulen Optimierungs-Flag -O statt -O2 anwenden, weil Letzteres kaputten Code erzeugt; Bau-Fehlschlag auf arm64 durch Abschaltung von PIE für Fortran-Programme behoben">
<correction choose-mirror "Enthält aktualisierte Spiegelliste">
<correction chrony "Protokollierung von Messungen und Statistiken und das Anhalten von chronyd auf Plattformen mit aktivierter seccomp-Filterung überarbeitet">
<correction ckermit "OpenSSL-Versionsprüfung entfernt">
<correction clamav "Haldenzugriff außerhalb der Grenzen beim Scannen von PDF-Dokumenten [CVE-2019-1787], mit Aspack gepackten PE-Dateien [CVE-2019-1789] oder OLE2-Dateien [CVE-2019-1788] behoben">
<correction dansguardian "<q>missingok</q> der logrotate-Konfiguration hinzufügen">
<correction debian-installer "Neubau gegen proposed-updates">
<correction debian-installer-netboot-images "Neubau gegen proposed-updates">
<correction debian-security-support "Support-Status aktualisiert">
<correction diffoscope "Tests überarbeitet, damit sie mit Ghostscript 9.26 zusammenarbeiten">
<correction dns-root-data "Root-Daten auf 2019031302 aktualisiert">
<correction dnsruby "Neuer root-Key (KSK-2017); ruby 2.3.0 missbilligt TimeoutError, stattdessen Timeout::Error benutzen">
<correction dpdk "Neue Stable-Veröffentlichung der Originalautoren">
<correction edk2 "Pufferüberlauf im BlockIo-Dienst behoben [CVE-2018-12180]; DNS: Vor Verwendung empfangener Pakete ihre Größe prüfen [CVE-2018-12178]; Stapelüberlauf bei korrumpiertem BMP behoben [CVE-2018-12181]">
<correction firmware-nonfree "atheros / iwlwifi: neue Bluetooth-Firmware [CVE-2018-5383]">
<correction flatpak "Alle ioctls, die der Kernel als TIOCSTI interpretieren wird, abweisen [CVE-2019-10063]">
<correction geant321 "Neubau gegen cernlib mit überarbeiteten Fortran-Optimierungen">
<correction gnome-chemistry-utils "Aufhören, das obsolete gcu-plugin-Paket zu packen">
<correction gocode "gocode-auto-complete-el: auto-complete-el zur Vor-Abhängigkeit hochstufen, um den Erfolg von Upgrades sicherzustellen">
<correction gpac "Pufferüberläufe behoben [CVE-2018-7752 CVE-2018-20762], genauso Haldenüberläufe [CVE-2018-13005 CVE-2018-13006 CVE-2018-20761] und Schreibvorgänge außerhalb der Grenzen [CVE-2018-20760 CVE-2018-20763]">
<correction icedtea-web "Browser-Plugin nicht mehr bauen, funktioniert nicht mehr mit Firefox 60">
<correction igraph "Absturz beim Laden von fehlerhaften GraphML-Dateien behoben [CVE-2018-20349]">
<correction jabref "XML External Entity-Angriff verhindert [CVE-2018-1000652]">
<correction java-common "default-java-plugin-Paket entfernen, da das icedtea-web-Xul-Plugin ebenfalls entfernt wird">
<correction jquery "Object.prototype-Verschmutzung verhindern [CVE-2019-11358]">
<correction kauth "Unsichere Handhabung von Argumenten in Hilfsprogrammen behoben [CVE-2019-7443]">
<correction libdate-holidays-de-perl "Den achten März (ab 2019) und den achten Mai (nur 2020) den öffentlichen Feiertagen hinzugefügt (nur Berlin)">
<correction libdatetime-timezone-perl "Enthaltene Daten aktualisiert">
<correction libreoffice "Neue japanische Gengou-Ära 'Reiwa' hinterlegt; dafür gesorgt, dass -core einen Konflikt mit openjdk-8-jre-headless (= 8u181-b13-2~deb9u1) verursacht, bei dem ein ClassPathURLCheck defekt ist">
<correction linux "Neue Stable-Version der Originalautoren">
<correction linux-latest "Für -9 Kernel-ABI aktualisiert">
<correction mariadb-10.1 "Neue Stable-Version der Originalautoren">
<correction mclibs "Neubau gegen cernlib mit überarbeiteten Fortran-Optimierungen">
<correction ncmpc "Nullzeiger-Dereferenzierung behoben [CVE-2018-9240]">
<correction node-superagent "ZIP-Bomb-Attacken abgestellt [CVE-2017-16129]; Syntaxfehler behoben">
<correction nvidia-graphics-drivers "Neue Stable-Veröffentlichung der Originalautoren [CVE-2018-6260]">
<correction nvidia-settings "Neue Stable-Veröffentlichung der Originalautoren">
<correction obs-build "Nicht erlauben, in Dateien auf dem Wirtssystem zu schreiben [CVE-2017-14804]">
<correction paw "Neubau gegen cernlib mit überarbeiteten Fortran-Optimierungen">
<correction perlbrew "HTTPS-CPAN-URLs erlauben">
<correction postfix "Neue Stable-Veröffentlichung der Originalautoren">
<correction postgresql-9.6 "Neue Stable-Veröffentlichung der Originalautoren">
<correction psk31lx "Dafür sorgen, dass version korrekt sortiert, um mögliche Upgrade-Probleme zu verhindern">
<correction publicsuffix "Enthaltene Daten aktualisiert">
<correction pyca "<q>missingok</q> der Logrotate-Konfiguration hinzugefügt">
<correction python-certbot "Auf debhelper compat 9 zurückkehren, damit systemd-Timer korrekt gestartet werden">
<correction python-cryptography "BIO_callback_ctrl entfernt: Der Protoyp unterscheidet sich von dem, was in OpenSSL definiert ist, nachdem es in OpenSSL überarbeitet wurde">
<correction python-django-casclient "django 1.10 Middleware-Korrektur angewandt; python(3)-django-casclient: fehlende Abhängigkeiten von python(3)-django nachgetragen">
<correction python-mode "Unterstützung für xemacs21 entfernt">
<correction python-pip "HTTPError der HTTP-Requests korrekt in index.py abfangen">
<correction python-pykmip "Potenzielles Dienstblockade-Problem behoben [CVE-2018-1000872]">
<correction r-cran-igraph "Dienstblockade durch handgemachtes Objekt behoben [CVE-2018-20349]">
<correction rails "Informations-Offenlegungen [CVE-2018-16476 CVE-2019-5418] und Dienstblockaden [CVE-2019-5419] abgestellt">
<correction rsync "Mehrere Sicherheitskorrekturen für zlib [CVE-2016-9840 CVE-2016-9841 CVE-2016-9842 CVE-2016-9843]">
<correction ruby-i18n "Anfälligkeiten für Dienstblockaden aus der Ferne behoben [CVE-2014-10077]">
<correction ruby2.3 "FTBFS korrigiert">
<correction runc "Anfälligkeit für root-Privilegien-Eskalation entfernt [CVE-2019-5736]">
<correction systemd "journald: Assertionsfehler in journal_file_link_data behoben; Temporärdateien: <q>e</q> überarbeitet, um Shell-Style-Globs zu unterstützen; mount-util: akzeptieren, dass name_to_handle_at() mit EPERM fehlschlagen kann; automount: automount-Anfragen bestätigen, auch wenn schon eingehängt [CVE-2018-1049]; potenzielle root-Privilegien-Eskalation behoben [CVE-2018-15686]">
<correction twitter-bootstrap3 "Cross-Site-Skripting-Probleme in Tooltipps und Popovers behoben [CVE-2019-8331]">
<correction tzdata "Neue Version der Originalautoren">
<correction unzip "Pufferüberlauf in passwortgeschützten ZIP-Archiven behoben [CVE-2018-1000035]">
<correction vcftools "Informationsoffenlegung [CVE-2018-11099] und Dienstblockade [CVE-2018-11129 CVE-2018-11130] durch handgeschriebene Dateien behoben">
<correction vips "Null-Funktionszeigerdereferenzierung [CVE-2018-7998] und Zugriff auf nicht initialisierten Speicher [CVE-2019-6976] behoben">
<correction waagent "Neue Veröffentlichung der Originalautoren mit vielen Azure-Korrekturen [CVE-2019-0804]">
<correction yorick-av "Einzelbild-Zeitstempel neu skalieren; VBV-Puffergröße für MPEG1/2-Dateien festlegen">
<correction zziplib "Unzulässigen Speicherzugriff [CVE-2018-6381], Busfehler [CVE-2018-6540], Lesezugriff außerhalb der Grenzen [CVE-2018-7725], Absturz durch handgemachte ZIP-Datei [CVE-2018-7726] und Speicherleck [CVE-2018-16548] behoben; ZIP-Datei abweisen, falls die Größe des zentralen Verzeichnisses und/oder der Versatz seines Anfangs über das Ende der ZIP-Datei hinausreichen [CVE-2018-6484, CVE-2018-6541, CVE-2018-6869]">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für jede 
davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2018 4259 ruby2.3>
<dsa 2018 4332 ruby2.3>
<dsa 2018 4341 mariadb-10.1>
<dsa 2019 4373 coturn>
<dsa 2019 4374 qtbase-opensource-src>
<dsa 2019 4377 rssh>
<dsa 2019 4385 dovecot>
<dsa 2019 4387 openssh>
<dsa 2019 4388 mosquitto>
<dsa 2019 4389 libu2f-host>
<dsa 2019 4390 flatpak>
<dsa 2019 4391 firefox-esr>
<dsa 2019 4392 thunderbird>
<dsa 2019 4393 systemd>
<dsa 2019 4394 rdesktop>
<dsa 2019 4396 ansible>
<dsa 2019 4397 ldb>
<dsa 2019 4398 php7.0>
<dsa 2019 4399 ikiwiki>
<dsa 2019 4400 openssl1.0>
<dsa 2019 4401 wordpress>
<dsa 2019 4402 mumble>
<dsa 2019 4403 php7.0>
<dsa 2019 4405 openjpeg2>
<dsa 2019 4406 waagent>
<dsa 2019 4407 xmltooling>
<dsa 2019 4408 liblivemedia>
<dsa 2019 4409 neutron>
<dsa 2019 4410 openjdk-8>
<dsa 2019 4411 firefox-esr>
<dsa 2019 4412 drupal7>
<dsa 2019 4413 ntfs-3g>
<dsa 2019 4414 libapache2-mod-auth-mellon>
<dsa 2019 4415 passenger>
<dsa 2019 4416 wireshark>
<dsa 2019 4417 firefox-esr>
<dsa 2019 4418 dovecot>
<dsa 2019 4419 twig>
<dsa 2019 4420 thunderbird>
<dsa 2019 4422 apache2>
<dsa 2019 4423 putty>
<dsa 2019 4424 pdns>
<dsa 2019 4425 wget>
<dsa 2019 4426 tryton-server>
<dsa 2019 4427 samba>
<dsa 2019 4428 systemd>
<dsa 2019 4429 spip>
<dsa 2019 4430 wpa>
<dsa 2019 4431 libssh2>
<dsa 2019 4432 ghostscript>
<dsa 2019 4433 ruby2.3>
<dsa 2019 4434 drupal7>
</table>

<h2>Entfernte Pakete</h2>
<p>
Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb unserer 
Kontrolle liegen:
</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction gcontactsync "Inkompatibel mit neueren firefox-esr-Versionen">
<correction google-tasks-sync "Inkompatibel mit neueren firefox-esr-Versionen">
<correction mozilla-gnome-kerying "Inkompatibel mit neueren firefox-esr-Versionen">
<correction tbdialout "Inkompatibel mit neueren thunderbird-Versionen">
<correction timeline "Inkompatibel mit neueren thunderbird-Versionen">

</table>

<h2>Debian-Installer</h2>

<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch diese Zwischenveröffentlichung in Stable eingeflossen sind.
</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert 
haben:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata 
usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software, 
die ihre Kraft und Zeit einbringen, um das vollständig freie Betriebssystem 
Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail an 
&lt;press@debian.org&gt;, oder kontaktieren das Stable-Release-Team 
auf Englisch über &lt;debian-release@lists.debian.org&gt;.</p>
