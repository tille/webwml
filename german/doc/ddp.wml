#use wml::debian::template title="Debian-Dokumentations-Projekt (DDP)" MAINPAGE="true"
#use wml::debian::translation-check translation="5aec2f5899ebe3a7d392d8cf19a28e5e02673b7f"
# $Id$
# Translated by Thomas Strathmann <thomas.strathmann@t-online.de>
# Updated: Erik Pfannenstein <debianignatz@gmx.de>, 2023.
# Updated: Holger Wansing <hwansing@mailbox.org>, 2023.

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#ddp_work">Unsere Arbeit</a></li>
</ul>


<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Das Debian-Dokumentations-Projekt (DDP) kümmert sich
   um Debians Dokumentation, zum Beispiel die Benutzer- und Entwickler-Handbücher, verschiedene Anleitungen,
   FAQs und die Veröffentlichungshinweise.
   Diese Seite gibt einen kurzen Überblick über die Arbeit des DDP.</p>
</aside>

<h2><a id="ddp_work">Unsere Arbeit</h2>

<p>
<div class="line">
  <div class="item col50">

    <h3>Handbücher</h3>
    <ul>
      <li><strong><a href="user-manuals">Benutzer-Handbücher</a></strong></li>
      <li><strong><a href="devel-manuals">Entwickler-Handbücher</a></strong></li>
      <li><strong><a href="misc-manuals">Verschiedene weitere Handbücher</a></strong></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h3>Dokumentations-Richtlinien</h3>
    <ul>
      <li>Handbuch-Lizenzen sind konform mit der DFSG.</li>
      <li>Wir verwenden Docbook XML für unsere Dokumente.</li>
      <li>Alle Quelltexte sind in unserem <a href="https://salsa.debian.org/ddp-team">Git-Depot</a> verfügbar.</li>
      <li><tt>www.debian.org/doc/&lt;handbuchname&gt;</tt> ist die offizielle URL.</li>
      <li>Jedes Dokument sollte aktiv betreut werden.</li>
    </ul>

    <h3>Git-Zugriff</h3>
    <ul>
      <li>Wie Sie auf das <a href="vcs">Git-Depot des DDP</a> zugreifen</li>
    </ul>

  </div>


</div>
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-envelope fa-2x"></span> Mit dem Team in Kontakt treten über <a href="https://lists.debian.org/debian-doc/">debian-doc</a></button></p>
