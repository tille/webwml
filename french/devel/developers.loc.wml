#use wml::debian::template title="Emplacement des développeurs"
#use wml::debian::translation-check translation="bd2e76d96db915ccd0dee367a373417db7422565" maintainer="Jean-Pierre Giraud"
# Original translation by Denis Barbier

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Où sont les développeurs Debian (DD) ? Si un DD a indiqué les coordonnées de son lieu de résidence dans la base de données des développeurs, il est visible sur la carte du monde.</p>
</aside>

<p>
La carte ci-dessous a été générée par le programme
<a href="https://packages.debian.org/stable/graphics/xplanet">xplanet</a>
à partir de la <a href="developers.coords">liste anonymisée des coordonnées
des développeurs</a>.
</p>

<img src="developers.map.jpeg" alt="World Map">

<h2>Comment ajouter ses coordonnées</h2>

<p>
Si vous êtes un développeur et souhaitez voir vos coordonnées
ajoutées à la base de données, entrez dans la
<a href="https://db.debian.org">base de données des développeurs Debian</a>
et modifiez le champ correspondant. Si vous ne connaissez pas les coordonnées
de la ville où vous habitez, vous pouvez utiliser
<a href="https://www.openstreetmap.org">OpenStreetMap</a> pour les trouver. Recherchez votre
ville et sélectionnez les flèches de direction à côté de la case « Recherche ».
Faites glisser le repère vert situé à gauche de la case « De » sur la carte
d'OSM et les coordonnées apparaissent dans la case.
</p>

<p>Le format pour les coordonnées est un des suivants :</p>

<dl>
  <dt>degrés décimaux</dt>
    <dd>Le format est <code>+-DDD.DDDDDDDDDDDDDDD</code>. C'est le format
    utilisé par des programmes comme Xearth ainsi que par de nombreux sites de
    positionnement. Cependant, la précision est souvent limitée à 4 ou
    5 décimales.</dd>
  <dt>degrés minutes (DGM)</dt>
    <dd>Le format est <code>+-DDDMM.MMMMMMMMMMMMM</code>. Ce n'est pas un
    type arithmétique, mais une représentation compacte de deux unités
    distinctes, les degrés et les minutes. Cette sortie est courante dans
    certains types d'appareils GPS portables et dans les messages des GPS
    au format NMEA.
  <dt>degrés minutes secondes (DGMS)</dt>
    <dd>Le format est <code>+-DDDMMSS.SSSSSSSSSSS</code>. Comme pour les DGM,
    ce n'est pas un type arithmétique, mais une représentation compacte de
    trois unités distinctes : les degrés, les minutes et les secondes. Cette
    sortie vient typiquement de sites web qui donnent trois valeurs pour
    chaque position. Par exemple, si la position fournie est
    <code>34:50:12.24523 Nord</code>, elle est <code>+0345012.24523</code>
    en DGMS.</dd>
</dl>

<p>
<strong>Remarque :</strong> pour la latitude, <code>+</code> est le nord, pour
la longitude, <code>+</code> est l'est. Il est important de spécifier
suffisamment de zéros au début de la position pour être sûr de supprimer les
ambiguïtés si la position est à moins de 2 degrés du point 0.
</p>
