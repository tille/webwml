#use wml::debian::translation-check translation="1e2e8f73c9a0918a76ace7b9844fe4ba20b68e48" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40982">CVE-2022-40982</a>

<p>Daniel Moghimi a découvert la faille « Gather Data Sampling » (GDS),
une vulnérabilité matérielle pour les processeurs Intel qui permettait un
accès spéculatif non privilégié à des données qui étaient précédemment
stockées dans les registres de vecteurs.</p>

<p>Cette mitigation a besoin du microcode mis à jour du processeur fournit
dans le paquet intel-microcode.</p>

<p>Pour plus de détails, veuillez vous référer aux pages
<a href="https://downfall.page/">https://downfall.page/</a> et
<a href="https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/gather-data-sampling.html">\
https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/gather-data-sampling.html</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-20569">CVE-2023-20569</a>

<p>Daniel Trujillo, Johannes Wikner et Kaveh Razavi ont découvert la faille
INCEPTION, connue également sous le nom de « Speculative Return Stack
Overflow » (SRSO), une attaque par exécution transitoire qui divulgue des
données arbitraires sur tous les processeurs Zen d'AMD. Un attaquant peut
apprendre de façon erronée au « Branch Target Buffer » (BTB) du processeur
à prédire des instructions CALL non liées à l'architecture dans l'espace du
noyau et utiliser cela pour contrôler la cible spéculative d'une
instruction subséquente RET du noyau, menant éventuellement à la
divulgation d'informations à l'aide d'une attaque spéculative par canal
auxiliaire.</p>

<p>Pour plus de détails, veuillez vous référer aux pages
<a href="https://comsec.ethz.ch/research/microarch/inception/">\
https://comsec.ethz.ch/research/microarch/inception/</a> et
<a href="https://www.amd.com/en/corporate/product-security/bulletin/amd-sb-7005">\
https://www.amd.com/en/corporate/product-security/bulletin/amd-sb-7005</a>.</p></li>

</ul>

<p>Pour la distribution oldstable (Bullseye), ces problèmes ont été
corrigés dans la version 5.10.179-5.</p>

<p>Pour la distribution stable (Bookworm), ces problèmes ont été corrigés
dans la version 6.1.38-4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5475.data"
# $Id: $
