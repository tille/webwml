#use wml::debian::translation-check translation="65fff6f71fc4f2c8b699391f0855d86ed805b96b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les utilisateurs authentifiés de l'API d'Orthanc, un serveur DICOM pour
l'imagerie médicale, pouvaient écraser des fichiers arbitraires et, dans
certaines configurations, exécuter du code arbitraire.</p>

<p>Cette mise à jour rétroporte l'option RestApiWriteToFileSystemEnabled,
la définissant à <q>true</q> dans /etc/orthanc/orthanc.json et restaure
le comportement antérieur.</p>

<p>Pour la distribution oldstable (Bullseye), ce problème a été corrigé
dans la version 1.9.2+really1.9.1+dfsg-1+deb11u1.</p>

<p>Pour la distribution stable (Bookworm), ce problème a été corrigé dans
la version 1.10.1+dfsg-2+deb12u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets orthanc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de orthanc, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/orthanc">\
https://security-tracker.debian.org/tracker/orthanc</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5473.data"
# $Id: $
