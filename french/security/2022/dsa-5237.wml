#use wml::debian::translation-check translation="056c7d3e1ab716c9f4f43368e7a895525176f3a9" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes de sécurité ont été découverts dans le navigateur
web Firefox de Mozilla. Cela pouvait éventuellement avoir pour conséquences
l'exécution de code arbitraire, le contournement du <q>Content Security
Policy</q> (CSP) ou une fixation de session.</p>

<p>Debian suit les éditions longue durée (ESR, <q>Extended Support
Release</q>) de Firefox. Le suivi des séries 91.x est terminé, aussi, à
partir de cette mise à jour, Debian suit les versions 102.x.</p>

<p>Entre les versions 91.x et 102.x, Firefox a vu nombre de ses fonctions
mises à jour. Pour davantage d'informations, veuillez consulter la page
<a href="https://www.mozilla.org/en-US/firefox/102.0esr/releasenotes/">\
https://www.mozilla.org/en-US/firefox/102.0esr/releasenotes/</a>.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 102.3.0esr-1~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firefox-esr.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de firefox-esr,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/firefox-esr">\
https://security-tracker.debian.org/tracker/firefox-esr</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5237.data"
# $Id: $
