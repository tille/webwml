#use wml::debian::translation-check translation="bda50db949a220ada94593e8cf3e932c33bc890f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Matthieu Barjole et Victor Cutillas ont découvert que sudoedit dans sudo, un
programme conçu pour procurer des privilèges limités de superutilisateur à
certains utilisateurs, gérait incorrectement <q>--</q> pour séparer l’éditeur
des arguments des fichiers à éditer. Un utilisateur local autorisé à éditer
certains fichiers pouvait exploiter ce défaut pour modifier un fichier indûment
selon la politique de sécurité, aboutissant à une élévation des privilèges.</p>

<p>Plus d’informations sont disponibles sur la page :
<a href="https://www.sudo.ws/security/advisories/sudoedit_any/">https://www.sudo.ws/security/advisories/sudoedit_any/</a>.</p>


<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.8.27-1+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sudo.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sudo,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sudo">\
https://security-tracker.debian.org/tracker/sudo</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3272.data"
# $Id: $
