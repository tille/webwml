#use wml::debian::translation-check translation="c89d5a1786cb9d838189a90104dc43b55e913911" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>open-vm-tools est un paquet fournissant des outils d’Open VMware pour
des machines virtuelles hébergées sur VMware.</p>

<p>Il a été découvert que des outils d’Open VM géraient incorrectement certaines
requêtes d’authentification. Un hôte ESXi complètement compromis pouvait forcer
des outils d’Open VM à échouer à authentifier des opérations hôte vers invité,
impactant la confidentialité et l’intégrité de la machine virtuelle invitée.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2:10.3.10-1+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets open-vm-tools.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de open-vm-tools,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/open-vm-tools">\
https://security-tracker.debian.org/tracker/open-vm-tools</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3531.data"
# $Id: $
