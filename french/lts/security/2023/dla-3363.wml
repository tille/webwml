#use wml::debian::translation-check translation="02e596b9ab402970b6536a73588a5e7176e3c4ca" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de lecture hors limites ont été trouvées dans pcre2,
une bibliothèque d’expressions rationnelles compatible avec Perl, qui pouvaient
aboutir à une divulgation d'informations ou un déni ou service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20454">CVE-2019-20454</a>

<p>Lecture hors limites quand le motif <code>&bsol;X</code> était compilé à la
volée et utilisé pour correspondre à des sujets contrefaits pour l'occasion dans
le mode non UTF.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1586">CVE-2022-1586</a>

<p>Lecture hors limites impliquant des propriétés Unicode correspondant aux
expressions rationnelles compilées à la volée. Ce problème se produisait parce
que le caractère n’était pas entièrement lu dans la correspondance sans casse lors
de la compilation à la volée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1587">CVE-2022-1587</a>

<p>Lecture hors limites affectant les récursions dans les expressions
rationnelles compilées à la volée causée par des transferts de données
dupliqués.</p>

<p>Cet envoi corrige aussi une lecture excessive de tampon de sujet lors de la
compilation à la volée quand UTF est désactivé et <code>&bsol;X</code> ou
<code>&bsol;R</code> ont un quantificateur fixe supérieur à 1. Ce problème a été
découvert par Yunho Kim.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 10.32-5+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pcre2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de pcre2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/pcre2">\
https://security-tracker.debian.org/tracker/pcre2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3363.data"
# $Id: $
