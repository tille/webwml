#use wml::debian::translation-check translation="0e723a9634239be6387f01354845243fb781cb88" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour inclut les modifications dans tzdata 2023b. Les plus
importantes sont :</p>

<ul>
<li>l’Égypte utilise de nouveau l’heure d’été à partir d’avril ;</li>
<li>la Palestine et le Liban ont remis l’heure d’été à plus tard cette année ;</li>
<li>l’heure d’été au Maroc débutera une semaine plus tôt en avril 23 ;</li>
<li>ajustements des règles de fuseau horaire et d’heure d’été au Groenland.</li>
</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2021a-0+deb10u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tzdata.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tzdata,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tzdata">\
https://security-tracker.debian.org/tracker/tzdata</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3366.data"
# $Id: $
