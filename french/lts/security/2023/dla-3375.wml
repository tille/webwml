#use wml::debian::translation-check translation="4491163b377fc66ad3b3aad91f3c76fab770c796" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un certain nombre de vulnérabilités dans
le serveur RDP (Remote Protocol Desktop) xrdb :</p>

<ul>

<li>
<a href="https://security-tracker.debian.org/tracker/CVE-2022-23480">CVE-2022-23480</a>:
prévention d’une série de vulnérabilités potentielles de dépassement de
tampon dans la fonction devredir_proc_client_devlist_announce_req() ;</li>

<li>
<a href="https://security-tracker.debian.org/tracker/CVE-2022-23481">CVE-2022-23481</a>:
correction d’une vulnérabilité de lecture hors limites dans la fonction
xrdp_caps_process_confirm_active() ;</li>

<li>
<a href="https://security-tracker.debian.org/tracker/CVE-2022-23480">CVE-2022-23480</a>:
correction d’une vulnérabilité de lecture hors limites dans la fonction
xrdp_sec_process_mcs_data_CS_CORE().</li>
</ul>
<p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.9.9-1+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xrdp.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3375.data"
# $Id: $
