#use wml::debian::translation-check translation="e305869c43b1a72aed1e6b8cdcd171419fb9434f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation des privilèges, un déni de service ou à
une fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2873">CVE-2022-2873</a>

<p>Zheyu Ma a découvert qu’un défaut d’accès en mémoire hors limites dans le
pilote du contrôleur d’hôte d’Intel iSMT SMBus 2.0 pouvait aboutir à un déni
de service (plantage du système).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3424">CVE-2022-3424</a>

<p>Zheng Wang et Zhuorao Yang ont signalé un défaut dans le pilote GRU SGI
qui pouvait conduire à une utilisation de mémoire après libération. Sur les
systèmes où ce pilote était utilisé, un utilisateur local pouvait exploiter cela
pour un déni de service (plantage ou corruption de mémoire) ou, éventuellement,
pour une élévation des privilèges.</p>

<p>Ce pilote n’est pas activé dans les configurations officielles du noyau dans
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3545">CVE-2022-3545</a>

<p>Il a été découvert que le pilote NFP (Netronome Flow Processor) contenait
un défaut d’utilisation de mémoire après libération dans area_cache_get(). Cela
pouvait permettre l’exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3707">CVE-2022-3707</a>

<p>Zheng Wang a signalé un défaut dans la prise en charge de la virtualisation
du pilote graphique i915(GVT-g), qui pouvait conduire à une double libération
de zone de mémoire. Sur les systèmes où cette fonctionnalité était utilisée,
un invité pouvait exploiter cela pour un déni de service (plantage ou corruption
de mémoire) ou, éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4744">CVE-2022-4744</a>

<p>L’outil syzkaller a trouvé un défaut dans le pilote réseau TUN/TAP, qui
pouvait conduire à une double libération de zone de mémoire. Un utilisateur
local pouvait exploiter cela pour un déni de service (plantage ou corruption de
mémoire) ou, éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36280">CVE-2022-36280</a>

<p>Une vulnérabilité d’écriture en mémoire hors limites a été découverte dans
le pilote vmwgfx, qui pouvait permettre à un utilisateur local non privilégié
de provoquer un déni de service (plantage du système).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41218">CVE-2022-41218</a>

<p>Hyunwoo Kim a signalé un défaut d’utilisation de mémoire après libération
dans le sous-système central Media DVB provoqué par des concurrences refcount,
qui pouvait permettre à un utilisateur local de provoquer un déni de service ou
une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-45934">CVE-2022-45934</a>

<p>Un dépassement d'entier dans l2cap_config_req() dans le sous-système
Bluetooth a été découvert, qui pouvait permettre à un attaquant physiquement
proche de provoquer un déni de service (plantage du système).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47929">CVE-2022-47929</a>

<p>Frederick Lawler a signalé un déréférencement de pointeur NULL dans le
sous-système de contrôle du trafic, permettant à un utilisateur non privilégié
de provoquer un déni de service en réglant une configuration de contrôle de
trafic contrefaite pour l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0045">CVE-2023-0045</a>

<p>Rodrigo Branco et Rafael Correa De Ysasi ont signalé qu’une tâche en espace
utilisateur demandait au noyau d’activer la mitigation de Spectre v2, Cette
mitigation n’était pas activée avant que la tâche ne soit reprogrammée. Cela
pouvait être exploitable par un attaquant local ou distant pour divulguer des
informations sensibles à partir d’une telle application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0266">CVE-2023-0266</a>

<p>Une défaut d’utilisation de mémoire après libération dans le sous-système son
à cause d’un verrou manquant pouvait aboutir à un déni de service ou à une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0394">CVE-2023-0394</a>

<p>Kyle Zeng a découvert un défaut de déréférencement de pointeur NULL dans
rawv6_push_pending_frames() dans le sous-système réseau, permettant à un
utilisateur local de provoquer un déni de service (plantage du système).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0458">CVE-2023-0458</a>

<p>Jordy Zimmer et Alexandra Sandulescu ont trouvé que getrlimit() et les
appels système relatifs étaient vulnérables à des attaques d’exécution
spéculative telles que Spectre v1. Un utilisateur local pouvait exploiter cela
pour une divulgation d’informations sensibles du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0459">CVE-2023-0459</a>

<p>Jordy Zimmer et Alexandra Sandulescu ont trouvé une régression dans la
mitigation de Spectre v1 dans les fonctions <q>user-copy</q> pour l’architecture
amd64 (PC 64 bits). Quand les CPU ne mettaient pas en œuvre SMAP ou que celui-ci
était désactivé, un utilisateur local pouvait exploiter cela pour une
divulgation d’informations sensibles du noyau. D’autres architectures pouvaient
aussi être affectées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0461">CVE-2023-0461</a>

<p><q>slipper</q> a signalé un défaut dans la prise en charge du noyau des ULP
(Upper protocole Layer) au-dessus de TCP, qui pouvait conduire à une double
libération de zone de mémoire lors de l’utilisation des sockets TLS du noyau.
Un utilisateur local pouvait exploiter cela pour un déni de service (plantage
ou corruption de mémoire) ou, éventuellement, pour une élévation des privilèges.</p>

<p>TLS du noyau n’est pas activé dans les configurations officielles du noyau
dans Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1073">CVE-2023-1073</a>

<p>Pietro Borrello a signalé un défaut de confusion de type dans le sous-système
HID (Human Interface Device). Un attaquant capable d’insérer et de retirer des
périphériques USB pouvait utiliser cela pour provoquer un déni de service
(plantage ou corruption de mémoire) ou, éventuellement, pour exécuter du code
arbitraire dans le noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1074">CVE-2023-1074</a>

<p>Pietro Borrello a signalé un défaut de confusion de type dans
l’implémentation du protocole SCTP, qui pouvait à une fuite de mémoire. Un
utilisateur local pouvait exploiter cela pour provoquer un déni de service
(épuisement de ressources).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1078">CVE-2023-1078</a>

<p>Pietro Borrello a signalé un défaut de confusion de type dans
l’implémentation du protocole RDS. Un utilisateur local pouvait exploiter cela
pour provoquer un déni de service (plantage ou corruption de mémoire) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1079">CVE-2023-1079</a>

<p>Pietro Borrello a signalé une situation de compétition dans le pilote HID
hid-asus, qui pouvait conduire à une utilisation de mémoire après libération.
Un attaquant capable d’insérer ou de retirer des périphériques USB pouvait
utiliser cela pour provoquer un déni de service (plantage ou corruption de
mémoire) ou, éventuellement, pour exécuter du code arbitraire dans le noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1118">CVE-2023-1118</a>

<p>Duoming Zhou a signalé une situation de compétition dans le pilote de
contrôle distant ene_ir, qui pouvait conduire à une utilisation de mémoire après
libération si le pilote était détaché. L’impact de sécurité de ce problème
n’est pas très défini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1281">CVE-2023-1281</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2023-1829">CVE-2023-1829</a>

<p><q>valis</q> a signalé deux défauts dans le classificateur de trafic réseau
cls_tcindex network, qui pouvaient conduire à une utilisation de mémoire après
libération. Un utilisateur local pouvait exploiter cela pour une élévation des
privilèges. Cette mise à jour retire complètement cls_tcindex.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1513">CVE-2023-1513</a>

<p>Xingyuan Mo a signalé une fuite d'informations dans l’implémentation de KVM
pour l’architecture i386 (PC 32 bits). Un utilisateur local pouvait exploiter
cela pour divulguer des informations sensibles du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1670">CVE-2023-1670</a>

<p>Zheng Wang a signalé une situation de compétition dans le pilote réseau
xirc2ps_cs network, qui pouvait conduire à une utilisation de mémoire après
libération. Un attaquant capable d’insérer ou de retirer des périphériques PMCIA
pouvait utiliser cela pour provoquer un déni de service (plantage ou corruption
de mémoire) ou, éventuellement, pour exécuter du code arbitraire dans le noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1855">CVE-2023-1855</a>

<p>Zheng Wang a signalé une situation de compétition dans le pilote de
supervision matérielle xgene-hwmon, qui pouvait conduire à une utilisation de
mémoire après libération. L’impact de sécurité de ce problème n’est pas très
défini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1859">CVE-2023-1859</a>

<p>Zheng Wang a signalé une situation de compétition dans le transport 9pnet_xen
pour le système de fichiers 9P dans Xen, qui pouvait conduire à une utilisation
de mémoire après libération. Dans les systèmes où cette fonctionnalité était
utilisée, un pilote de dorsal dans un autre domaine pouvait utiliser cela pour
provoquer un déni de service (plantage ou corruption de mémoire) ou,
éventuellement, pour exécuter du code arbitraire dans le domaine vulnérable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1989">CVE-2023-1989</a>

<p>Zheng Wang a signalé une situation de compétition dans le pilote btsdio
d’adaptateur Bluetooth, qui pouvait conduire à une utilisation de mémoire après
libération. Un attaquant capable d’insérer ou de retirer des périphériques SDIO
pouvait utiliser cela pour provoquer un déni de service (plantage ou corruption
de mémoire) ou, éventuellement, pour exécuter du code arbitraire dans le noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1990">CVE-2023-1990</a>

<p>Zheng Wang a signalé une situation de compétition dans le pilote st-nci
d’adaptateur NFC, pouvait conduire à une utilisation de mémoire après
libération. L’impact de sécurité de ce problème n’est pas très
défini.</p>

<p>Ce pilote n’est pas activé dans les configurations officielles du noyau
dans Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1998">CVE-2023-1998</a>

<p>José Oliveira et Rodrigo Branco ont signalé une régression dans la
mitigation de Spectre v2 pour l’espace utilisateur pour les CPU x86 gérant
IBRS mais pas eIBRS. Cela pouvait être exploitable par un attaquant local ou
distant pour divulguer des informations sensibles d’une application en espace
utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2162">CVE-2023-2162</a>

<p>Mike Christie a signalé une situation de compétition dans le transport iSCSI
TCP, qui pouvait conduire à une utilisation de mémoire après libération. Dans
les systèmes où cette fonctionnalité était utilisée, un utilisateur local
pouvait utiliser cela pour provoquer un déni de service (plantage ou corruption
de mémoire) ou, éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2194">CVE-2023-2194</a>

<p>Wei Chen a signalé un dépassement potentiel de tampon de tas dans le pilote
i2c-xgene-slimpro d’adaptateur IÂ²C. Un utilisateur local avec permission
d’accès à de tels périphériques, pouvait utiliser cela pour provoquer un déni
de service (plantage ou corruption de mémoire) et, probablement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23454">CVE-2023-23454</a>

<p>Kyle Zeng a signalé que l’ordonnanceur réseau CBQ (Class Based Queueing)
était prédisposé à un déni de service à cause de l’interprétation de
classification aboutissait avant la vérification du code de retour de la
classification.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23455">CVE-2023-23455</a>

<p>Kyle Zeng a signalé que l’ordonnanceur réseau de circuits virtuels (ATM)
était prédisposé à un déni de service à cause de l’interprétation de
classification aboutissait avant la vérification du code de retour de la
classification.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23559">CVE-2023-23559</a>

<p>Szymon Heidrich a signalé une vérification incorrecte de limites le pilote
rndis_wlan Wi-Fi, qui pouvait conduire à un dépassement de tampon de tas ou
une lecture hors limites. Un attaquant capable d’insérer ou de retirer des
périphériques USB pouvait utiliser cela pour provoquer un déni de service
(plantage ou corruption de mémoire) ou une fuite d'informations, ou,
éventuellement, pour exécuter du code arbitraire dans le noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-26545">CVE-2023-26545</a>

<p>Lianhui Tang a signalé un défaut dans l’implémentation du protocole MPLS, qui
pouvait conduire à une double libération de zone de mémoire. Un utilisateur
local pouvait exploiter cela pour provoquer un déni de service (plantage ou
corruption de mémoire) ou, possiblement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28328">CVE-2023-28328</a>

<p>Wei Chen a signalé un défaut dans le pilote az6927 DVB, qui pouvait conduire
à un déréférencement de pointeur NULL. Un utilisateur local pouvant accéder
à un périphérique adaptateur IÂ²C que ce pilote créait pouvait utiliser cela
pour provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-30456">CVE-2023-30456</a>

<p>Reima ISHII a signalé un défaut dans l’implémentation de KVM pour les CPU
d’Intel affectant la virtualisation imbriquée. Quand KVM était utilisé comme
hyperviseur L0 et que EPT et/ou le mode invité non restreint était désactivé,
cela n’empêchait pas un invité L2 d’être configuré avec mode protection/paging
architecturellement non valable. Un invité malfaisant pouvait exploiter cela
pour provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-30772">CVE-2023-30772</a>

<p>Zheng Wang a signalé une situation de compétition dans de pilote da9150
de chargeur, qui pouvait conduire à une utilisation de mémoire après libération.
L’impact de sécurité de ce problème n’est pas très défini.</p>


<p>Ce pilote n’est pas activé dans les configurations officielles du noyau
dans Debian.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 4.19.282-1. De plus, cette mise à jour corrige le bogue
<a href="https://bugs.debian.org/825141">n° 825141</a>
et inclut beaucoup de corrections de bogue à partir des mises à jour, incluant
la version 4.19.270-4.19.282.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3403.data"
# $Id: $
