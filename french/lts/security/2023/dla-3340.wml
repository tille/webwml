#use wml::debian::translation-check translation="4c5af89ea5029d9bbb99a00aecd39ebdc7ad989c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été découverte dans libgit2, une implémentation de
bibliothèque de Git, multiplateforme et connectable, qui pouvait aboutir à une
exécution de code à distance lors du clonage d’un dépôt sur un système de
fichiers de type NTFS ou à des attaques de type « homme du milieu », à cause
d’une vérification incorrecte de la signature de chiffrement.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12278">CVE-2020-12278</a>

<p>Un problème a été découvert dans libgit2 avant les versions 0.28.4 et 0.9x
avant 0.99.0. path.c gère incorrectement des noms de fichier équivalents qui
existent à cause de flux de données alternatifs de NTFS. Cela pouvait permettre
une exécution de code à distance lors du clonage d’un dépôt.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12279">CVE-2020-12279</a>

<p>Un problème a été découvert dans libgit2 avant les versions 0.28.4 et 0.9x
avant 0.99.0. checkout.c gère incorrectement les noms de fichier équivalents qui
existent à cause de noms courts de NTFS.  Cela pouvait permettre une exécution
de code à distance lors du clonage d’un dépôt.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-22742">CVE-2023-22742</a>

<p>libgit2 est une implémentation de bibliothèque de Git, multiplateforme et
connectable. Lors de l’utilisation d’un SSH distant avec le dorsal facultatif
libssh2, libgit2 par défaut ne réalisait pas de vérification de certificat. Les
versions précédentes de libgit2 requéraient de l’appelant de définir le champ
<q>certificate_check</q> de la structure <q>git_remote_callbacks</q> de libgit2
— si un rappel pour vérification de certificat n’était pas défini, libgit2 ne
réalisait aucune vérification de certificat. Cela signifiait que par défaut,
sans configuration de cette fonction de rappel, les clients ne réalisaient pas
de validation des clés SSH sur le serveur et pouvaient être sujet à une attaque
d’homme du milieu.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.27.7+dfsg.1-0.2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libgit2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libgit2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libgit2">\
https://security-tracker.debian.org/tracker/libgit2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3340.data"
# $Id: $
