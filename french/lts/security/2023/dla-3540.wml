#use wml::debian::translation-check translation="7b8952da8d6be9fe439d022567b8518ccf9957a6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un autoblocage pouvait se produire pour un en-tête non fiable X-Forwarded-For
dans MediaWiki, un moteur de site web pour du travail collaboratif.</p>

<p>X-Forwarded-For n’est pas nécessairement digne de confiance et peut indiquer
plusieurs adresses IP dans un seul en-tête, toutes étant vérifiées pour des
blocages. Quand un utilisateur est autobloqué, le wiki crée un blocage d’IP
en arrière-plan pour cet utilisateur sans exposer son IP sur le wiki. Cependant,
une usurpation d’XFF pouvait permettre à un attaquant de deviner l’IP des
utilisateurs ayant des autoblocages, puisque le message de blocage incluait
le nom d’utilisateur de la cible originelle de blocage.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:1.31.16-1+deb10u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mediawiki.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mediawiki,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/mediawiki">\
https://security-tracker.debian.org/tracker/mediawiki</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3540.data"
# $Id: $
