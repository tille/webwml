#use wml::debian::translation-check translation="99450b937d36464f9f23baab7252a2da8a2a8dd8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour corrige plusieurs vulnérabilités de validation de format de
fichier qui pouvaient aboutir à des violations d’accès mémoire telles que des
dépassements de tampon et des exceptions de virgule flottante. Il corrige aussi
une régression dans l’analyse de hcom introduite dans la correction du
<a href="https://security-tracker.debian.org/tracker/CVE-2017-11358">CVE-2017-11358</a>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13590">CVE-2019-13590</a>

<p>Dans <code>sox-fmt.h</code> (fonction startread), un dépassement d'entier
existait dans le résultat de l’addition d’entiers (arrondissement à 0) fourni
dans la macro <code>lsx_calloc</code> qui enveloppe <code>malloc</code>. Quand
un pointeur <code>NULL</code> était renvoyé, il était utilisé sans vérification
préalable de sa validité, conduisant à un déréférencement de pointeur
<code>NULL</code> dans <code>lsx_readbuf</code> dans <code>formats_i.c</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3643">CVE-2021-3643</a>

<p>La fonction <code>lsx_adpcm_init</code> dans libsox conduisait à un
dépassement de tampon global. Ce défaut permettait à un attaquant d’entrer un
fichier malveillant, conduisant à une divulgation d’informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23159">CVE-2021-23159</a>

<p>Une vulnérabilité a été découverte dans SoX, où un dépassement de tampon de
tas se produisait dans la fonction <code>lsx_read_w_buf()</code> dans le fichier
<code>formats_i.c</code>. La vulnérabilité était exploitable avec un
fichier contrefait qui pouvait causer un plantage d’application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23172">CVE-2021-23172</a>

<p>Une vulnérabilité a été découverte dans SoX, où un dépassement de tampon de
tas se produisait dans la fonction <code>startread()</code> dans le fichier
<code>hcom.c</code>. La vulnérabilité était exploitable avec un fichier hcomn
contrefait qui pouvait causer un plantage d’application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23210">CVE-2021-23210</a>

<p>Un problème d’exception de virgule flottante (division par zéro) a été
découvert dans SoX dans la fonction <code>read_samples()</code> du fichier
<code>voc.c</code>. Un attaquant avec un fichier contrefait pouvait causer un
plantage d’application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33844">CVE-2021-33844</a>

<p>Un problème d’exception de virgule flottante a été découvert dans SoX dans
la fonction <code>startread()</code> du fichier <code>wav.c</code>. Un attaquant
avec un fichier contrefait pouvait causer un plantage d’application.</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40426">CVE-2021-40426</a>

<p>Une vulnérabilité de dépassement de tampon de tas existait dans la fonction
<code>sphere.c</code> de <code>start_read()</code> de libsox de Sound Exchange.
Un fichier contrefait pour l'occasion pouvait conduire à un dépassement de
tampon de tas. Un attaquant pouvait fournir un fichier malveillant pour
déclencher cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31650">CVE-2022-31650</a>

<p>Une exception de virgule flottante existait dans
<code>lsx_aiffstartwrite</code> dans <code>aiff.c</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31651">CVE-2022-31651</a>

<p>Un échec d’assertion existait dans <code>rate_init</code> dans
<code>rate.c</code>.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 14.4.2+git20190427-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sox.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sox,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sox">\
https://security-tracker.debian.org/tracker/sox</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3315.data"
# $Id: $
