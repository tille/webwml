#use wml::debian::translation-check translation="ab68531186cd29ea8640257cc97b10fe716d621d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Apache Traffic Server,
un serveur et mandataire inverse.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47184">CVE-2022-47184</a>

<p>La méthode TRACE pouvait être utilisée pour divulguer des informations sur
le réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-30631">CVE-2023-30631</a>

<p>L’option de configuration pour bloquer la méthode PUSH dans ATS ne
fonctionnait pas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-33933">CVE-2023-33933</a>

<p>Problème avec le greffon s3_auth pour le calcul du hachage.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 8.1.7-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets trafficserver.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de trafficserver,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/trafficserver">\
https://security-tracker.debian.org/tracker/trafficserver</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3475.data"
# $Id: $
