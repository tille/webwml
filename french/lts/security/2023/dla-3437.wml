#use wml::debian::translation-check translation="69135f5f7271c63d2b1641e099b363fbe69d1b0c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes de sécurité ont été découverts dans libssh, une petite
bibliothèque C SSH, qui pouvaient permettre à un utilisateur distant authentifié
de provoquer un déni de service ou d’injecter des commandes arbitraires.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14889">CVE-2019-14889</a>

<p>Un défaut a été découvert dans la fonction ssh_scp_new() de l’IPA libssh dans
les versions avant 0.9.3 et avant 0.8.8. Quand le client SCP libssh se
connectait au serveur, la commande scp, qui incluait un chemin fourni par
l’utilisateur, était exécutée du côté serveur. Dans le cas où la bibliothèque
était utilisée d’une façon permettant aux utilisateurs d’agir sur le troisième
paramètre de la fonction, il devenait possible pour un attaquant d’injecter
des commandes arbitraires, conduisant à une compromission de la cible distante.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1667">CVE-2023-1667</a>

<p>Un déréférencement de pointeur NULL a été découvert dans libssh durant le
rechargement de clé en devinant l’algorithme. Ce problème pouvait permettre
à un client authentifié de provoquer un déni de service.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.8.7-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libssh.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libssh,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libssh">\
https://security-tracker.debian.org/tracker/libssh</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3437.data"
# $Id: $
