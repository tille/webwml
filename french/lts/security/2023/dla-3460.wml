#use wml::debian::translation-check translation="bd755a02b83b419190392b01553831e4c29be27a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Erik Krogh Kristensen et Rasmus Petersen du <q>GitHub Security Lab</q> ont
découvert une vulnérabilité ReDoS (déni de service par expression rationnelle)
dans python-mechanize, une bibliothèque pour automatiser l’interaction avec des
sites web modélisée d’après le module Perl <code>WWW::Mechanize</code>, qui
pouvait conduire à un déni de service lors de l’analyse d’un en-tête
d’authentification mal formé.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:0.2.5-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-mechanize.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-mechanize,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-mechanize">\
https://security-tracker.debian.org/tracker/python-mechanize</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3460.data"
# $Id: $
