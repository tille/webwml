#use wml::debian::translation-check translation="afc3bc4d554d05cdc0020842d2d316f450bb5f51" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un défaut a été découvert dans keepalived, un démon de surveillance et de
basculement pour les grappes LVS, dans lequel une vulnérabilité
d’authentification incorrecte permettait à un utilisateur non privilégié de
modifier des propriétés, ce qui pouvait conduire à un contournement du
contrôle d’accès.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:2.0.10-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets keepalived.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de keepalived,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/keepalived">\
https://security-tracker.debian.org/tracker/keepalived</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3388.data"
# $Id: $
