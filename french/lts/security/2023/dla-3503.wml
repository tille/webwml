#use wml::debian::translation-check translation="2b6d1aecc4716f0ef5056e9769d5e5e9b7b8ebb0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans les extensions du cadriciel
média GStreamer et de ses codecs et démultiplexeurs, qui pouvaient aboutir à
un déni de service ou éventuellement à l’exécution de code arbitraire si un
fichier média mal formé était ouvert.</p>


<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.14.4-1+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gst-plugins-bad1.0.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de gst-plugins-bad1.0,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/gst-plugins-bad1.0">\
https://security-tracker.debian.org/tracker/gst-plugins-bad1.0</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3503.data"
# $Id: $
