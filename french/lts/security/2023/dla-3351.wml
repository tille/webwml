#use wml::debian::translation-check translation="3e57bf3c3001e8356eb60f28cca599cdc5889a23" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans le serveur HTTP
Apache.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2006-20001">CVE-2006-20001</a>

<p>Un en-tête de requête <q>If:</q> soigneusement contrefait pouvait provoquer
une lecture de mémoire, ou l’écriture d’un seul octet zéro, dans un emplacement
de mémoire (tas) après la valeur de l’en-tête envoyé. Cela pouvait planter
le processus.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33193">CVE-2021-33193</a>

<p>Une méthode contrefaite envoyée à travers HTTP/2 pouvait contourner la
validation et être retransmise par mod_proxy, ce qui pouvait conduire à une
division de requête ou un empoisonnement du cache.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36760">CVE-2022-36760</a>

<p>Une vulnérabilité d’interprétation de requêtes HTTP (<q>dissimulation de
requête HTTP</q>) dans mod_proxy_ajp du serveur HTTP Apache permettait à un
attaquant de dissimuler des requêtes au serveur AJP auquel elles étaient
adressées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37436">CVE-2022-37436</a>

<p>Un dorsal malveillant pouvait provoquer le troncage d’en-têtes prématurément,
aboutissant à l’incorporation de certains en-têtes dans le corps de la réponse.
Si les en-têtes de fin existaient dans un but de sécurité, ils n'étaient pas
interprétés par le client.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.4.38-3+deb10u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apache2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de apache2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/apache2">\
https://security-tracker.debian.org/tracker/apache2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3351.data"
# $Id: $
