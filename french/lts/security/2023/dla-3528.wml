#use wml::debian::translation-check translation="abf84b9a592537ec164d77dc3b71f13474789c7d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été corrigées dans poppler, une bibliothèque de rendu
de PDF.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36023">CVE-2020-36023</a>

<p>Boucle infinie dans FoFiType1C::cvtGlyph().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36024">CVE-2020-36024</a>

<p>Déréférencement de pointeur NULL dans FoFiType1C::convertToType1().</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.71.0-5+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets poppler.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de poppler,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/poppler">\
https://security-tracker.debian.org/tracker/poppler</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3528.data"
# $Id: $
