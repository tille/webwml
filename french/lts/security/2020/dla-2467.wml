#use wml::debian::translation-check translation="bc000e17dbe2d3bbab40536a4b340eef6d97e81d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19787">CVE-2018-19787</a>

<p>Il a été découvert qu’il existait une vulnérabilité d’injection de script
intersite (XSS) dans la bibliothèque de manipulation HTML/XSS LXML pour Python.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27783">CVE-2020-27783</a>

<p>Protection de javascript à l’aide de combinaisons &lt;noscript&gt; et
&lt;style&gt;.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 3.7.1-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets lxml.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de lxml, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/lxml">https://security-tracker.debian.org/tracker/lxml</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2467.data"
# $Id: $
