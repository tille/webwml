#use wml::debian::translation-check translation="fb0d1c16c8295b2df92b980c9e3fd059ed23a68f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait une vulnérabilité potentielle de traversée de répertoires dans
l’<em>app</em> de Rack::Directory fournie avec Rack.</p>

<p>Si certains répertoires existent dans un répertoire géré par Rack::Directory,
un attaquant peut, en utilisant cette vulnérabilité, lire le contenu de fichiers
sur le serveur en dehors de la racine précisée lors de l’initialisation de
Rack::Directory.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 1.5.2-3+deb8u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-rack.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2216.data"
# $Id: $
