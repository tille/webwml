#use wml::debian::translation-check translation="384ae701cfbda3952b2db9ed63c00e753a88b1a6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été trouvés dans SLURM (Simple Linux Utility for
Resource Management), un système de gestion de ressources et de planification de
travaux pour l’informatique dématérialisée.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6438">CVE-2019-6438</a>

<p>SchedMD Slurm gère incorrectement les systèmes 32 bits, provoquant un
dépassement de tas dans xmalloc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12838">CVE-2019-12838</a>

<p>SchedMD Slurm ne protège pas les chaînes lors de l’importation d’un fichier
d’archive dans le dorsal pour accounting_storage/mysql, aboutissant à une
injection SQL.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 14.03.9-5+deb8u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets slurm-llnl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2143.data"
# $Id: $
