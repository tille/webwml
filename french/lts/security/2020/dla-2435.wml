#use wml::debian::translation-check translation="c2628aa83fbb5db21ee75d0514f5e614ffceb029" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le composant serveur d’Apache Guacamole, une passerelle pour accéder à des
bureaux distants, ne validait pas correctement les données reçues de serveurs
RDP. Cela pouvait aboutir à une divulgation d'informations ou même à l'exécution
de code arbitraire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9497">CVE-2020-9497</a>

<p>Apache Guacamole ne validait pas correctement les données reçues de serveurs
RDP à l’aide de canaux statiques virtuels. Si un utilisateur se connectait à un
serveur RDP malveillant ou corrompu, des PDU contrefaits pour l'occasion
pourraient aboutir à une divulgation d’informations de la mémoire du processus
guacd gérant la connexion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9498">CVE-2020-9498</a>

<p>Apache Guacamole pouvait mal gérer les pointeurs impliqués dans le traitement
de données reçues à l’aide de canaux statiques virtuels RDP. Si un utilisateur
se connectait à un serveur RDP malveillant ou corrompu, des séries de PDU
contrefaits pour l'occasion pourraient aboutir à une corruption de la mémoire,
permettant éventuellement l’exécution de code arbitraire avec les privilèges du
processus exécutant guacd.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 0.9.9-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets guacamole-server.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de guacamole-server, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/guacamole-server">https://security-tracker.debian.org/tracker/guacamole-server</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2435.data"
# $Id: $
