#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Grégoire Scano"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans libvncserver, une bibliothèque de création/d'intégration de serveur VNC :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9941">CVE-2016-9941</a>

<p>Correction d'un dépassement de tampon basé sur le tas qui permet à des serveurs distants
de provoquer un déni de service à l'aide d'un message FramebufferUpdate contrefait
contenant un sous-rectangle en dehors de la zone de dessin.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9942">CVE-2016-9942</a>

<p>Correction d'un dépassement de tampon basé sur le tas qui permet à des serveurs distants
de provoquer un déni de service à l'aide d'un message FramebufferUpdate contrefait avec
une tuile de type <q>Ultra</q> telle que la donnée LZO décompressée dépasse la
taille des dimensions de la tuile.</p></li>

</ul>

<p> Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été résolus dans la
version 0.9.9+dfsg-1+deb7u2 de libvncserver.</p>

<p>Nous vous recommandons de mettre à jour vos paquets serveur libvn.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-777.data"
# $Id: $
