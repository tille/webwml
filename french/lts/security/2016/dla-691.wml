#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4658">CVE-2016-4658</a>

<p>Les nœuds d'espace de nommage peuvent être copiés pour éviter des erreurs
d'utilisation de mémoire après libération. Mais ils n'ont pas
nécessairement une représentation physique dans un document, aussi, il
suffit de les désactiver dans les intervalles de XPointer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5131">CVE-2016-5131</a>

<p>L'ancien code invoquait la fonction xmlXPtrRangeToFunction.
<q>range-to</q> n'est pas réellement une fonction mais un type spécial
d'étape de position. Il faut retirer cette fonction et toujours gérer
<q>range-to</q> dans le code de XPath. L'ancienne fonction
xmlXPtrRangeToFunction pourrait être aussi abusée pour déclencher une erreur
d'utilisation de mémoire après libération avec une potentialité d'exécution
distante de code.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2.8.0+dfsg1-7+wheezy7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxml2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-691.data"
# $Id: $
