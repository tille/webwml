#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que Tor traitait le contenu de certains morceaux de
tampon comme si c'était une chaîne terminée par un caractère NUL. Ce
problème pourrait permettre à un attaquant distant de planter un client, un
service caché, un relais ou une autorité Tor. Cette mise à jour vise à se
défendre contre cette classe générale de bogues.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ce problème a été corrigé dans la
version 0.2.4.27-2.</p>

<p>Pour la distribution stable (Jessie), ce problème a été corrigé dans la
version 0.2.5.12-3., pour unstable (Sid) avec la version 0.2.8.9-1, et pour
experimental avec la version 0.2.9.4-alpha-1.</p>

<p>En complément, pour <q>Wheezy</q>, cela met à jour l'ensemble des
serveurs de répertoires d'autorité vers celui venant de Tor 0.2.8.7, publié
en août 2016.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tor.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-663.data"
# $Id: $
