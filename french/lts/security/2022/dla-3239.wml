#use wml::debian::translation-check translation="725291ca5f94ed23481ba10d6665e5aaa42d7216" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été trouvés dans Git, un système de gestion de
versions décentralisé. Un attaquant pouvait amener d’autres utilisateurs à
exécuter des commandes arbitraires, divulguer des informations sur le système
de fichiers local et contourner l’interpréteur restreint.
<p><b>Remarque</b> : à cause des nouvelles restrictions de sécurité, l’accès aux
dépôts possédés et accédés par les différents utilisateurs locaux peut être
désormais rejeté par Git. Dans le cas où changer le propriétaire n’est pas
pratique, git affiche une façon de contourner ces vérifications en utilisant
la nouvelle entrée de configuration, <q>safe.directory</q>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24765">CVE-2022-24765</a>

<p>Git ne vérifiait pas le propriétaire de répertoires dans un système local
multiutilisateur lors de l’exécution de commandes indiquées dans la
configuration locale du dépôt. Cela permettait au propriétaire du dépôt de
provoquer l’exécution de commandes arbitraires par d’autres utilisateurs
accédant au dépôt.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29187">CVE-2022-29187</a>

<p>Un utilisateur non méfiant pouvait toujours être affecté par le problème
signalé dans le
<a href="https://security-tracker.debian.org/tracker/CVE-2022-24765">CVE-2022-24765</a>, par exemple lors de la navigation comme superutilisateur dans un répertoire
tmp partagé qu’il possède, mais où un attaquant pouvait créer un dépôt git.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39253">CVE-2022-39253</a>

<p>Exposition d’informations sensibles à un acteur malveillant. Lors d’un
clonage local (où la source et la cible du clone sont sur le même volume), Git
copie le contenu du répertoire <q>$GIT_DIR/objects</q> de la source dans la
destination soit en créant des liens en dur vers le contenu de la source, soit
en le copiant (si les liens en dur sont désactivés à l’aide de
<q>--no-hardlinks</q>). Un acteur malveillant pouvait pousser une victime à
cloner un dépôt avec un lien symbolique pointant sur des informations sensibles
dans la machine de la victime.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39260">CVE-2022-39260</a>

<p><q>git shell</q> utilise incorrectement un <q>int</q> pour représenter le
nombre d’entrées dans le tableau, permettant à un acteur malveillant
d’intentionnellement outrepasser la valeur de retour, conduisant à une écriture de
tas arbitraire. Parce que le tableau résultant est alors passé à <q>execv()</q>,
il est possible d’exploiter cette attaque pour obtenir une exécution de code à
distance sur la machine de la victime.</p></li>

</ul>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 1:2.20.1-2+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets git.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de git,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/git">\
https://security-tracker.debian.org/tracker/git</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3239.data"
# $Id: $
