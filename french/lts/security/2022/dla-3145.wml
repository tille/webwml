#use wml::debian::translation-check translation="79a41c97fc4711590fbc7cca28392b72500cfb4f" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans Git, un
système de gestion de versions distribué, rapide et évolutif, qui peuvent
affecter les systèmes multiutilisateurs.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21300">CVE-2021-21300</a>

<p>Un dépôt contrefait pour l'occasion contenant des liens symboliques
ainsi que des fichiers utilisant un filtre <q>clean</q> ou <q>smudge</q>
tel que Git LFS, peut provoquer l'exécution d'un script qui vient d'être
récupéré tout en clonant un système de fichiers indifférent à la casse tel
que NTFS, HFS+ ou APFS (c'est-à-dire les systèmes de fichiers par défaut
sur Windows et macOS).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40330">CVE-2021-40330</a>

<p>git_connect_git dans connect.c permet qu'un chemin de dépôt contienne un
caractère de changement de ligne, ce qui peut avoir pour conséquence des
requêtes interprotocoles imprévues, comme démontré par la sous-chaîne
git://localhost:1234/%0d%0a%0d%0aGET%20/%20HTTP/1.1 .</p>


<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
1:2.20.1-2+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets git.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de git, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/git">\
https://security-tracker.debian.org/tracker/git</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3145.data"
# $Id: $
