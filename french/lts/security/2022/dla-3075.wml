#use wml::debian::translation-check translation="9a69809e2f1fd0203a5567a3b6bd48f8deabc9f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Julian Gilbey a découvert que schroot, un outil permettant aux
utilisateurs d'exécuter des commandes dans un <q>chroot</q> (environnement
fermé d'exécution), a des règles trop permissives sur le nom des chroots ou
des sessions, permettant un déni de service sur le service schroot pour
tous les utilisateurs qui peuvent lancer une session schroot.</p>

<p>Veuillez noter que les chroots et sessions existants sont vérifiés
pendant la mise à niveau et que la mise à niveau est interrompue si un nom
futur non valable est détecté.</p>

<p>Les chroots et sessions problématiques peuvent être vérifiés avant la
mise à niveau avec la commande suivante :</p>

<p>schroot --list --all | LC_ALL=C grep -vE '^[a-z]+:[a-zA-Z0-9][a-zA-Z0-9_.-]*$'</p>

<p>Consultez
<url "https://codeberg.org/shelter/reschroot/src/tag/release/reschroot-1.6.13/NEWS#L10-L41">
pour des instructions pour résoudre ce type de situation.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans la version
1.6.10-6+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets schroot.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de schroot, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/schroot">\
https://security-tracker.debian.org/tracker/schroot</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3075.data"
# $Id: $
