#use wml::debian::translation-check translation="1977bacdb1487210921e07e3da31720574bab9a2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans spamassassin, un filtre
anti-pourriel basé sur Perl et l'analyse de texte.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11805">CVE-2018-11805</a>

<p>Des fichiers de règles ou de configuration malveillants, éventuellement
téléchargés d'un serveur de mise à jour, pourraient exécuter des commandes
arbitraires selon plusieurs scénarios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12420">CVE-2019-12420</a>

<p>Des messages multi-parties contrefaits pour l'occasion peuvent faire que
spamassassin utilise des ressources excessives, avec pour conséquence un
déni de service.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.4.2-0+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets spamassassin.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2037.data"
# $Id: $
