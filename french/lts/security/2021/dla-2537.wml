#use wml::debian::translation-check translation="2e8936b6ebc10db0754290eecf70717b74ff9185" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans ffmpeg, un cadriciel multimedia
grandement utilisé.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17539">CVE-2019-17539</a>

<p>Un déréférencement de pointeur NULL et éventuellement un autre impact non
précisé en l’absence de pointeur valable de fin de fonction.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35965">CVE-2020-35965</a>

<p>Une écriture hors limites due à des erreurs de calcul lors de la réalisation
d’opérations memset avec zéro.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 7:3.2.15-0+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ffmpeg.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ffmpeg, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ffmpeg">https://security-tracker.debian.org/tracker/ffmpeg</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2537.data"
# $Id: $
