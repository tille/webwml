#use wml::debian::translation-check translation="5b1386f6fa8f762fa0618aaf308b9811ae60de93" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes ont été découverts dans curl, un outil en ligne de commande
et une bibliothèque côté client d’utilisation facile, pour transférer des
données avec une syntaxe d’URL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22946">CVE-2021-22946</a>

<p>Des réponses contrefaites du serveur pourraient obliger des clients à ne
pas utiliser TLS sur des connexions bien que TLS soit requis et attendu.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22947">CVE-2021-22947</a>

<p>Lors de l’utilisation de STARTTLS pour initialiser une connexion TLS,
le serveur pourrait envoyer plusieurs réponses avant la mise à niveau de TLS
de façon que le client les gère comme des réponses fiables. Cela pourrait être utilisé par
un attaquant de type homme du milieu pour injecter des données de réponse contrefaites.</p>


<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 7.52.1-5+deb9u16.</p>

<p>Nous vous recommandons de mettre à jour vos paquets curl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de curl,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2773.data"
# $Id: $
