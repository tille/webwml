<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that Flask, a lightweight WSGI web application
framework, will under certain conditions cache a response containing
data intended for one client and subsequently may send the response to
other clients.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.0.2-3+deb10u1.</p>

<p>We recommend that you upgrade your flask packages.</p>

<p>For the detailed security status of flask please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/flask">https://security-tracker.debian.org/tracker/flask</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3536.data"
# $Id: $
