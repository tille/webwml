<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in 389-ds-base: an open source LDAP
server for Linux.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3883">CVE-2019-3883</a>

    <p>SSL/TLS requests do not enforce ioblocktimeout limit, leading to DoS
    vulnerability by hanging all workers with hanging LDAP requests.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10224">CVE-2019-10224</a>

    <p>The vulnerability may disclose sensitive information, such as the Directory
    Manager password, when the dscreate and dsconf commands are executed in
    verbose mode. An attacker who can view the screen or capture the terminal
    standard error output can exploit thisvulnerability to obtain confidential information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14824">CVE-2019-14824</a>

    <p>The <q>deref</q> plugin of 389-ds-base has a vulnerability that enables it to
    disclose attribute values using the <q>search</q> permission. In certain setups,
    an authenticated attacker can exploit this flaw to access confidential
    attributes, including password hashes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3514">CVE-2021-3514</a>

    <p>If a sync_repl client is used, an authenticated attacker can trigger a crash
    by exploiting a specially crafted query that leads to a NULL pointer
    dereference.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3652">CVE-2021-3652</a>

    <p>Importing an asterisk as password hashes enables successful authentication
    with any password, allowing attackers to access accounts with disabled
    passwords.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4091">CVE-2021-4091</a>

    <p>A double free was found in the way 389-ds-base handles virtual attributes
    context in persistent searches. An attacker could send a series of search
    requests, forcing the server to behave unexpectedly, and crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0918">CVE-2022-0918</a>

    <p>An unauthenticated attacker with network access to the LDAP port can cause a
    denial of service. The denial of service is triggered by a single message
    sent over a TCP connection, no bind or other authentication is required. The
    message triggers a segmentation fault that results in slapd crashing.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0996">CVE-2022-0996</a>

    <p>Expired password was still allowed to access the database. A user whose
    password was expired was still allowed to access the database as if the
    password was not expired.  Once a password is expired, and <q>grace logins</q>
    have been used up, the account is basically supposed to be locked out and
    should not be allowed to perform any privileged action.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2850">CVE-2022-2850</a>

    <p>The vulnerability in content synchronization plugin enables an authenticated attacker to trigger a denial of service via a crafted query through a NULL
    pointer dereference.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.4.0.21-1+deb10u1.</p>

<p>We recommend that you upgrade your 389-ds-base packages.</p>

<p>For the detailed security status of 389-ds-base please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/389-ds-base">https://security-tracker.debian.org/tracker/389-ds-base</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3399.data"
# $Id: $
