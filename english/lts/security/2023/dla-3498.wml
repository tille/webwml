<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential denial of service (DoS)
in bind9, the popular Domain Name Server (DNS) server. Shoham Danino, Anat
Bremler-Barr, Yehuda Afek and Yuval Shavitt discovered that a flaw in the
cache-cleaning algorithm used in named can cause that named's configured cache
size limit can be significantly exceeded, potentially resulting in a denial of
service attack.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2828">CVE-2023-2828</a></li>
</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1:9.11.5.P4+dfsg-5.1+deb10u9.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3498.data"
# $Id: $
