<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Sebastien Meriot discovered that the S3 API of Swift, a distributed
virtual object store, was susceptible to information disclosure.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.19.1-1+deb10u1.</p>

<p>We recommend that you upgrade your swift packages.</p>

<p>For the detailed security status of swift please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/swift">https://security-tracker.debian.org/tracker/swift</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3281.data"
# $Id: $
