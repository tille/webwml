<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been found in qt4-x11, a graphical
windowing toolkit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3481">CVE-2021-3481</a>

    <p>While rendering and displaying a crafted Scalable Vector Graphics
    (SVG) file this flaw may lead to an unauthorized memory access. The
    highest threat from this vulnerability is to data confidentiality
    and the application availability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45930">CVE-2021-45930</a>

    <p>An out-of-bounds write in
    QtPrivate::QCommonArrayOps<QPainterPath::Element>::growAppend
    (called from QPainterPath::addPath and QPathClipper::intersect).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32573">CVE-2023-32573</a>

    <p>Uninitialized variable usage in m_unitsPerEm.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32763">CVE-2023-32763</a>

    <p>An application crash in QXmlStreamReader via a crafted XML string
    that triggers a situation in which a prefix is greater than a
    length.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-34410">CVE-2023-34410</a>

    <p>Certificate validation for TLS does not always consider whether the
    root of a chain is a configured CA certificate.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-37369">CVE-2023-37369</a>

    <p>There can be an application crash in QXmlStreamReader via a crafted
    XML string that triggers a situation in which a prefix is greater
    than a length.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38197">CVE-2023-38197</a>

    <p>There are infinite loops in recursive entity expansion.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
4:4.8.7+dfsg-18+deb10u2.</p>

<p>We recommend that you upgrade your qt4-x11 packages.</p>

<p>For the detailed security status of qt4-x11 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qt4-x11">https://security-tracker.debian.org/tracker/qt4-x11</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3539.data"
# $Id: $
