<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A buffer overrun in format_timespan() has been fixed
in systemd, the default init system in Debian.

Additionally, fixes for getting property OnExternalPower via D-Bus
and for a memory leak on daemon-reload are also included.</p>

<p>For Debian 10 buster, this problem has been fixed in version
241-7~deb10u10.</p>

<p>We recommend that you upgrade your systemd packages.</p>

<p>For the detailed security status of systemd please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/systemd">https://security-tracker.debian.org/tracker/systemd</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3474.data"
# $Id: $
