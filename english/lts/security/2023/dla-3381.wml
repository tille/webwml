<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential buffer-overflow
vulnerability in ghostscript, a popular interpreter for the
PostScript language used, for example, to generate PDF files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28879">CVE-2023-28879</a>

    <p>In Artifex Ghostscript through 10.01.0, there is a buffer overflow
    leading to potential corruption of data internal to the PostScript
    interpreter, in base/sbcp.c. This affects BCPEncode, BCPDecode, TBCPEncode,
    and TBCPDecode. If the write buffer is filled to one byte less than full,
    and one then tries to write an escaped character, two bytes are
    written.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
9.27~dfsg-2+deb10u7.</p>

<p>We recommend that you upgrade your ghostscript packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3381.data"
# $Id: $
