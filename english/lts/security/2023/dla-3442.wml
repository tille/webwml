<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Alvaro Muñoz from the GitHub Security Lab discovered sixteen ways to
exploit a cross-site scripting vulnerability in nbconvert, a tool and
library used to convert notebooks to various other formats via Jinja
templates.</p>

<p>When using nbconvert to generate an HTML version of a user-controllable
notebook, it is possible to inject arbitrary HTML which may lead to
cross-site scripting (XSS) vulnerabilities if these HTML notebooks are
served by a web server without tight <code>Content-Security-Policy</code>
(e.g., nbviewer).</p>

<p><ol>
  <li> GHSL-2021-1013: XSS in <code>notebook.metadata.language_info.pygments_lexer</code>;</li>
  <li> GHSL-2021-1014: XSS in <code>notebook.metadata.title</code>;</li>
  <li> GHSL-2021-1015: XSS in <code>notebook.metadata.widgets</code>;</li>
  <li> GHSL-2021-1016: XSS in <code>notebook.cell.metadata.tags</code>;</li>
  <li> GHSL-2021-1017: XSS in output data <code>text/html</code> cells;</li>
  <li> GHSL-2021-1018: XSS in output data <code>image/svg+xml</code> cells;</li>
  <li> GHSL-2021-1019: XSS in <code>notebook.cell.output.svg_filename</code>;</li>
  <li> GHSL-2021-1020: XSS in output data <code>text/markdown</code> cells;</li>
  <li> GHSL-2021-1021: XSS in output data <code>application/javascript</code> cells;</li>
  <li> GHSL-2021-1022: XSS in output.metadata.filenames <code>image/png</code> and <code>image/jpeg</code>;</li>
  <li> GHSL-2021-1023: XSS in output data <code>image/png</code> and <code>image/jpeg</code> cells;</li>
  <li> GHSL-2021-1024: XSS in output.metadata.width/height <code>image/png</code> and <code>image/jpeg</code>;</li>
  <li> GHSL-2021-1025: XSS in output data <code>application/vnd.jupyter.widget-state+json</code> cells;</li>
  <li> GHSL-2021-1026: XSS in output data <code>application/vnd.jupyter.widget-view+json</code> cells;</li>
  <li> GHSL-2021-1027: XSS in raw cells; and</li>
  <li> GHSL-2021-1028: XSS in markdown cells.</li>
</ol></p>

<p>Some of these vulnerabilities, namely GHSL-2021-1017, -1020, -1021 and -1028,
are actually design decisions where <code>text/html</code>, <code>text/markdown</code>,
<code>application/javascript</code> and markdown cells should allow for arbitrary
JavaScript code execution.  These vulnerabilities are therefore left open
by default, but users can now opt-out and strip down all JavaScript
elements via a new <code>HTMLExporter</code> option <code>sanitize_html</code>.</p>

<p>For Debian 10 buster, this problem has been fixed in version
5.4-2+deb10u1.</p>

<p>We recommend that you upgrade your nbconvert packages.</p>

<p>For the detailed security status of nbconvert please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/nbconvert">https://security-tracker.debian.org/tracker/nbconvert</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3442.data"
# $Id: $
