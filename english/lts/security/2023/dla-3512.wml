<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2156">CVE-2023-2156</a>

    <p>It was discovered that a flaw in the handling of the RPL protocol
    may allow an unauthenticated remote attacker to cause a denial of
    service if RPL is enabled (not by default in Debian).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3390">CVE-2023-3390</a>

    <p>A use-after-free flaw in the netfilter subsystem caused by
    incorrect error path handling may result in denial of service or
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3610">CVE-2023-3610</a>

    <p>A use-after-free flaw in the netfilter subsystem caused by
    incorrect refcount handling on the table and chain destroy path
    may result in denial of service or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-20593">CVE-2023-20593</a>

    <p>Tavis Ormandy discovered that under specific microarchitectural
    circumstances, a vector register in AMD <q>Zen 2</q> CPUs may not be
    written to 0 correctly.  This flaw allows an attacker to leak
    sensitive information across concurrent processes, hyper threads
    and virtualized guests.</p>

    <p>For details please refer to
    <a href="https://lock.cmpxchg8b.com/zenbleed.html">https://lock.cmpxchg8b.com/zenbleed.html</a> and
    <a href="https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8">https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8</a>.</p>

    <p>This issue can also be mitigated by a microcode update through the
    amd64-microcode package or a system firmware (BIOS/UEFI) update.
    However, the initial microcode release by AMD only provides
    updates for second generation EPYC CPUs.  Various Ryzen CPUs are
    also affected, but no updates are available yet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31248">CVE-2023-31248</a>

    <p>Mingi Cho discovered a use-after-free flaw in the Netfilter
    nf_tables implementation when using nft_chain_lookup_byid, which
    may result in local privilege escalation for a user with the
    CAP_NET_ADMIN capability in any user or network namespace.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-35001">CVE-2023-35001</a>

    <p>Tanguy DUBROCA discovered an out-of-bounds reads and write flaw in
    the Netfilter nf_tables implementation when processing an
    nft_byteorder expression, which may result in local privilege
    escalation for a user with the CAP_NET_ADMIN capability in any
    user or network namespace.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
5.10.179-3~deb10u1.</p>

<p>We recommend that you upgrade your linux-5.10 packages.</p>

<p>For the detailed security status of linux-5.10 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux-5.10">https://security-tracker.debian.org/tracker/linux-5.10</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3512.data"
# $Id: $
