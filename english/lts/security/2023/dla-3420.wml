<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An integer overflow vulnerability exists in golang-websocket, a Go package
implementing the WebSocket protocol connection. An attacker would use this flaw
to cause a denial of service attack on an HTTP Server allowing websocket
connections.</p>

<p>The following reverse-dependencies have been rebuilt against the new golangwebsocket version: hugo and gitlab-workhorse</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.4.0-1+deb10u1.</p>

<p>We recommend that you upgrade your golang-websocket packages.</p>

<p>For the detailed security status of golang-websocket please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/golang-websocket">https://security-tracker.debian.org/tracker/golang-websocket</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3420.data"
# $Id: $
