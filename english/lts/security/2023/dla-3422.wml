<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security issues were found in PostgreSQL, which may result in
privilege escalation or incorrect policy enforcement.</p>

<p>For Debian 10 buster, these problems have been fixed in version
11.20-0+deb10u1.</p>

<p>We recommend that you upgrade your postgresql-11 packages.</p>

<p>For the detailed security status of postgresql-11 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/postgresql-11">https://security-tracker.debian.org/tracker/postgresql-11</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3422.data"
# $Id: $
