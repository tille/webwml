<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Quadratic runtime with malformed PDFs missing xref marker has been fixed 
in PyPDF2, a pure Python PDF library.
</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.26.0-2+deb10u2.</p>

<p>We recommend that you upgrade your pypdf2 packages.</p>

<p>For the detailed security status of pypdf2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pypdf2">https://security-tracker.debian.org/tracker/pypdf2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3497.data"
# $Id: $
