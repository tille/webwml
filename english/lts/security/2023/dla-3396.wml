<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential remote denial of service
vulnerability in Redis, a popular 'NoSQL' key-value database.</p>

<p>Authenticated users could have used the <code>HINCRBYFLOAT</code> command to
create an invalid hash field that would have crashed the Redis server on
access.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28856">CVE-2023-28856</a>

    <p>Redis is an open source, in-memory database that persists on disk.
    Authenticated users can use the `HINCRBYFLOAT` command to create an invalid
    hash field that will crash Redis on access in affected versions. This issue
    has been addressed in in versions 7.0.11, 6.2.12, and 6.0.19. Users are
    advised to upgrade. There are no known workarounds for this issue.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
5:5.0.14-1+deb10u4.</p>

<p>We recommend that you upgrade your redis packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3396.data"
# $Id: $
