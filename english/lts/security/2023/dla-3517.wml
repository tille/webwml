<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A stack overflow in the MD5 function has been fixed in pdfcrack,
a tool for recovering passwords and content from PDF files.
</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.16-3+deb10u1.</p>

<p>We recommend that you upgrade your pdfcrack packages.</p>

<p>For the detailed security status of pdfcrack please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pdfcrack">https://security-tracker.debian.org/tracker/pdfcrack</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3517.data"
# $Id: $
