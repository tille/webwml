<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vunerabilities were discovered in openssl, a Secure Sockets Layer toolkit:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3446">CVE-2023-3446</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2023-3817">CVE-2023-3817</a></p>

    <p>Excessively long DH key or parameter checks can cause significant delays
    in applications using DH_check(), DH_check_ex(), or EVP_PKEY_param_check()
    functions, potentially leading to Denial of Service attacks when keys or
    parameters are obtained from untrusted sources.</p>


<p>For Debian 10 buster, these problems have been fixed in version
1.1.1n-0+deb10u6.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>For the detailed security status of openssl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssl">https://security-tracker.debian.org/tracker/openssl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3530.data"
# $Id: $
