<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An auto-block can occur for an untrusted X-Forwarded-For header in
MediaWiki, a website engine for collaborative work.</p>

<p>X-Forwarded-For is not necessarily trustworthy and can specify multiple IP
addresses in a single header, all of which are checked for blocks. When a user
is autoblocked, the wiki will create an IP block behind-the-scenes for that
user without exposing the user's IP on-wiki. However, spoofing XFF would let an
attacker guess at the IPs of users who have active autoblocks, since the
block message includes the username of the original block target.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1:1.31.16-1+deb10u6.</p>

<p>We recommend that you upgrade your mediawiki packages.</p>

<p>For the detailed security status of mediawiki please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/mediawiki">https://security-tracker.debian.org/tracker/mediawiki</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3540.data"
# $Id: $
