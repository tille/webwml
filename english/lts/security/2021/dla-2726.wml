<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there were two issues in shiro, a security
framework for Java applications:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13933">CVE-2020-13933</a>

    <p>Fix an authentication bypass resulting from a specially
    crafted HTTP request.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17510">CVE-2020-17510</a>

    <p>Fix an authentication bypass resulting from a specially
    crafted HTTP request.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.3.2-1+deb9u2.</p>

<p>We recommend that you upgrade your shiro packages.</p>

<p>For the detailed security status of shiro please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/shiro">https://security-tracker.debian.org/tracker/shiro</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2726.data"
# $Id: $
