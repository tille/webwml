<define-tag description>LTS security update</define-tag>
<define-tag moreinfo></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26519">CVE-2020-26519</a>

    <p>A heap-based buffer overflow flaw was discovered in MuPDF,
    a lightweight PDF viewer, which may result in denial of
    service or the execution of arbitrary code if malformed
    documents are opened.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3407">CVE-2021-3407</a>

    <p>A double free of object during linearization was discovered
    in MuPDF, a lightweight PDF viewer, which may lead to memory
    corruption and other potential consequences.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.9a+ds1-4+deb9u6.</p>

<p>We recommend that you upgrade your mupdf packages.</p>

<p>For the detailed security status of mupdf please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mupdf">https://security-tracker.debian.org/tracker/mupdf</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2589.data"
# $Id: $
