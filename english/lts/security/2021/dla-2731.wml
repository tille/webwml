<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>One security issue affects WordPress, a weblog manager, versions
between 3.7 and 5.7. This update fixes the following security issues:
Object injection in PHPMailer (<a href="https://security-tracker.debian.org/tracker/CVE-2020-36326">CVE-2020-36326</a> and <a href="https://security-tracker.debian.org/tracker/CVE-2018-19296">CVE-2018-19296</a>).</p>

<p>For Debian 9 stretch, this problem has been fixed in version
4.7.21+dfsg-0+deb9u1.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>For the detailed security status of wordpress please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wordpress">https://security-tracker.debian.org/tracker/wordpress</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2731.data"
# $Id: $
