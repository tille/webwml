<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Philipp Jeitner and Haya Shulman discovered a stack-based buffer
overflow in libspf2, a library for validating mail senders with SPF,
which could result in denial of service, or potentially execution of
arbitrary code when processing a specially crafted SPF record.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.2.10-7+deb9u1.</p>

<p>We recommend that you upgrade your libspf2 packages.</p>

<p>For the detailed security status of libspf2 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libspf2">https://security-tracker.debian.org/tracker/libspf2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2739.data"
# $Id: $
