<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple issues have been discovered in ffmpeg - tools for transcoding,
streaming and playing of multimedia files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-20445">CVE-2020-20445</a>

    <p>Divide By Zero issue via libavcodec/lpc.h, which allows a remote malicious
    user to cause a Denial of Service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-20446">CVE-2020-20446</a>

    <p>Divide By Zero issue via libavcodec/aacpsy.c, which allows a remote malicious
    user to cause a Denial of Service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-20451">CVE-2020-20451</a>

    <p>Denial of Service issue due to resource management errors via
    fftools/cmdutils.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-20453">CVE-2020-20453</a>

    <p>Divide By Zero issue via libavcodec/aaccoder, which allows a remote
    malicious user to cause a Denial of Service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22037">CVE-2020-22037</a>

    <p>A Denial of Service vulnerability due to a memory leak in
    avcodec_alloc_context3 at options.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22041">CVE-2020-22041</a>

    <p>A Denial of Service vulnerability due to a memory leak in
    the av_buffersrc_add_frame_flags function in buffersrc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22044">CVE-2020-22044</a>

    <p>A Denial of Service vulnerability due to a memory leak in the
    url_open_dyn_buf_internal function in libavformat/aviobuf.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22046">CVE-2020-22046</a>

    <p>A Denial of Service vulnerability due to a memory leak in the
    avpriv_float_dsp_allocl function in libavutil/float_dsp.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22048">CVE-2020-22048</a>

    <p>A Denial of Service vulnerability due to a memory leak in the
    ff_frame_pool_get function in framepool.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22049">CVE-2020-22049</a>

    <p>A Denial of Service vulnerability due to a memory leak in the
    wtvfile_open_sector function in wtvdec.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22054">CVE-2020-22054</a>

    <p>A Denial of Service vulnerability due to a memory leak in the av_dict_set
    function in dict.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38171">CVE-2021-38171</a>

    <p>adts_decode_extradata in libavformat/adtsenc.c does not check the
    init_get_bits return value, which is a necessary step because the second
    argument to init_get_bits can be crafted.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38291">CVE-2021-38291</a>

    <p>Assertion failure at src/libavutil/mathematics.c, causing ffmpeg aborted
    is detected. In some extreme cases, like with adpcm_ms samples with an
    extremely high channel count, get_audio_frame_duration() may return a
    negative frame duration value.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
7:3.2.16-1+deb9u1.</p>

<p>We recommend that you upgrade your ffmpeg packages.</p>

<p>For the detailed security status of ffmpeg please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ffmpeg">https://security-tracker.debian.org/tracker/ffmpeg</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2818.data"
# $Id: $
