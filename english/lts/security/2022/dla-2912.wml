<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that in libphp-adodb, a PHP database abstraction layer
library, an attacker can inject values into the PostgreSQL connection
string by bypassing adodb_addslashes(). The function can be bypassed
in phppgadmin, for example, by surrounding the username in quotes and
submitting with other parameters injected in between.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
5.20.9-1+deb9u1.</p>

<p>We recommend that you upgrade your libphp-adodb packages.</p>

<p>For the detailed security status of libphp-adodb please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libphp-adodb">https://security-tracker.debian.org/tracker/libphp-adodb</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2912.data"
# $Id: $
