<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that parsing errors in the mp4 module of Nginx, a
high-performance web and reverse proxy server, could result in denial
of service, memory disclosure or potentially the execution of arbitrary
code when processing a malformed mp4 file.</p>

<p>This module is only enabled in the nginx-extras binary package.</p>

<p>In addition the following vulnerability has been fixed.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3618">CVE-2021-3618</a>

    <p>ALPACA is an application layer protocol content confusion attack,
    exploiting TLS servers implementing different protocols but using
    compatible certificates, such as multi-domain or wildcard certificates.
    A MiTM attacker having access to victim's traffic at the TCP/IP layer can
    redirect traffic from one subdomain to another, resulting in a valid TLS
    session. This breaks the authentication of TLS and cross-protocol attacks
    may be possible where the behavior of one protocol service may compromise</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.14.2-2+deb10u5.</p>

<p>We recommend that you upgrade your nginx packages.</p>

<p>For the detailed security status of nginx please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/nginx">https://security-tracker.debian.org/tracker/nginx</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3203.data"
# $Id: $
