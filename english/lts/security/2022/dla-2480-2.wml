<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Past security updates of Salt, a remote execution manager, introduced
regressions for which follow-up fixes were published:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16846">CVE-2020-16846</a> regression

    <p><q>salt-ssh</q> master key initialization fails</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3197">CVE-2021-3197</a> regression

    <p>Valid parameters are discarded for the SSHClient</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28243">CVE-2020-28243</a> follow-up

    <p>Prevent argument injection in restartcheck</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25282">CVE-2021-25282</a> regression

    <p>pillar_roots.write cannot write to subdirs</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25284">CVE-2021-25284</a> regression

    <p>The <q>cmd.run</q> function crashes if passing tuple arg</p></li>

</ul>

<p>For Debian 9 stretch, this problem has been fixed in version
2016.11.2+ds-1+deb9u10.</p>

<p>We recommend that you upgrade your salt packages.</p>

<p>For the detailed security status of salt please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/salt">https://security-tracker.debian.org/tracker/salt</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2480-2.data"
# $Id: $
