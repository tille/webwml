<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that joblib did not properly sanitize arguments
to pre_dispatch, allowing arbitrary code execution.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.13.0-2+deb10u1.</p>

<p>We recommend that you upgrade your joblib packages.</p>

<p>For the detailed security status of joblib please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/joblib">https://security-tracker.debian.org/tracker/joblib</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3193.data"
# $Id: $
