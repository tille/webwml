<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in package atftp, an advanced TFTP client/server.</p>

<p>Due to missing bound checks, data could be read behind a buffer so that
sensible information might be disclosed to a remote client.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
0.7.git20120829-3.1~deb9u3.</p>

<p>We recommend that you upgrade your atftp packages.</p>

<p>For the detailed security status of atftp please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/atftp">https://security-tracker.debian.org/tracker/atftp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3028.data"
# $Id: $
