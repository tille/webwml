<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>dlt-daemon, a Diagnostic Log and Trace logging daemon, had the following
vulnerabilities reported:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29394">CVE-2020-29394</a>

    <p>A buffer overflow in the dlt_filter_load function in dlt_common.c
    from dlt-daemon allows arbitrary code execution because fscanf is
    misused (no limit on the number of characters to be read in the
    format argument).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36244">CVE-2020-36244</a>

    <p>dlt-daemon was vulnerable to a heap-based buffer overflow that
    could allow an attacker to remotely execute arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31291">CVE-2022-31291</a>

    <p>An issue in dlt_config_file_parser.c of dlt-daemon allows attackers
    to cause a double free via crafted TCP packets.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.18.0-1+deb10u1.</p>

<p>We recommend that you upgrade your dlt-daemon packages.</p>

<p>For the detailed security status of dlt-daemon please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/dlt-daemon">https://security-tracker.debian.org/tracker/dlt-daemon</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3231.data"
# $Id: $
