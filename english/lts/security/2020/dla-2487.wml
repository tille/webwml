<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that missing input validation in the ar/tar
implementations of APT, the high level package manager, could cause
out-of-bounds reads or infinite loops, resulting in denial of service
when processing malformed deb files.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.4.11.</p>

<p>We recommend that you upgrade your apt packages.</p>

<p>For the detailed security status of apt please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/apt">https://security-tracker.debian.org/tracker/apt</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2487.data"
# $Id: $
