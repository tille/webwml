<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in iperf3, an Internet Protocol bandwidth
measuring tool.
Bad handling of UTF8/16 strings in an embedded library could cause a
denial of service (crash) or execution of arbitrary code by putting
special characters in a JSON string, which triggers a heap-based buffer
overflow.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.0.7-1+deb8u1.</p>

<p>We recommend that you upgrade your iperf3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2080.data"
# $Id: $
