<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues have been discovered in PHP (recursive acronym for PHP:
Hypertext Preprocessor), a widely-used open source general-purpose
scripting language that is especially suited for web development and can
be embedded into HTML.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2554">CVE-2016-2554</a>

    <p>Stack-based buffer overflow in ext/phar/tar.c allows remote
    attackers to cause a denial of service (application crash) or
    possibly have unspecified other impact via a crafted TAR archive.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3141">CVE-2016-3141</a>

    <p>Use-after-free vulnerability in wddx.c in the WDDX extension allows
    remote attackers to cause a denial of service (memory corruption and
    application crash) or possibly have unspecified other impact by
    triggering a wddx_deserialize call on XML data containing a crafted
    var element.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3142">CVE-2016-3142</a>

    <p>The phar_parse_zipfile function in zip.c in the PHAR extension in
    PHP before 5.5.33 and 5.6.x before 5.6.19 allows remote attackers to
    obtain sensitive information from process memory or cause a denial
    of service (out-of-bounds read and application crash) by placing a
    PK\x05\x06 signature at an invalid location.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4342">CVE-2016-4342</a>

    <p>ext/phar/phar_object.c in PHP before 5.5.32, 5.6.x before 5.6.18,
    and 7.x before 7.0.3 mishandles zero-length uncompressed data, which
    allows remote attackers to cause a denial of service (heap memory
    corruption) or possibly have unspecified other impact via a crafted
    (1) TAR, (2) ZIP, or (3) PHAR archive.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9934">CVE-2016-9934</a>

    <p>ext/wddx/wddx.c in PHP before 5.6.28 and 7.x before 7.0.13 allows
    remote attackers to cause a denial of service (NULL pointer
    dereference) via crafted serialized data in a wddxPacket XML
    document, as demonstrated by a PDORow string.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9935">CVE-2016-9935</a>

    <p>The php_wddx_push_element function in ext/wddx/wddx.c in PHP before
    5.6.29 and 7.x before 7.0.14 allows remote attackers to cause a
    denial of service (out-of-bounds read and memory corruption) or
    possibly have unspecified other impact via an empty boolean element
    in a wddxPacket XML document.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10158">CVE-2016-10158</a>

    <p>The exif_convert_any_to_int function in ext/exif/exif.c in PHP
    before 5.6.30, 7.0.x before 7.0.15, and 7.1.x before 7.1.1 allows
    remote attackers to cause a denial of service (application crash)
    via crafted EXIF data that triggers an attempt to divide the minimum
    representable negative integer by -1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10159">CVE-2016-10159</a>

    <p>Integer overflow in the phar_parse_pharfile function in
    ext/phar/phar.c in PHP before 5.6.30 and 7.0.x before 7.0.15 allows
    remote attackers to cause a denial of service (memory consumption or
    application crash) via a truncated manifest entry in a PHAR archive.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10160">CVE-2016-10160</a>

    <p>Off-by-one error in the phar_parse_pharfile function in
    ext/phar/phar.c in PHP before 5.6.30 and 7.0.x before 7.0.15 allows
    remote attackers to cause a denial of service (memory corruption) or
    possibly execute arbitrary code via a crafted PHAR archive with an
    alias mismatch.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10161">CVE-2016-10161</a>

    <p>The object_common1 function in ext/standard/var_unserializer.c in
    PHP before 5.6.30, 7.0.x before 7.0.15, and 7.1.x before 7.1.1
    allows remote attackers to cause a denial of service (buffer
    over-read and application crash) via crafted serialized data that is
    mishandled in a finish_nested_data call.</p></li>

<li>BUG-71323

    <p>Output of stream_get_meta_data can be falsified by its input</p></li>

<li>BUG-70979

    <p>Crash on bad SOAP request</p></li>

<li>BUG-71039

    <p>exec functions ignore length but look for NULL termination</p></li>

<li>BUG-71459

    <p>Integer overflow in iptcembed()</p></li>

<li>BUG-71391

    <p>NULL Pointer Dereference in phar_tar_setupmetadata()</p></li>

<li>BUG-71335

    <p>Type confusion vulnerability in WDDX packet deserialization</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.4.45-0+deb7u7.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-818.data"
# $Id: $
