<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in wordpress, a web blogging
tool. The Common Vulnerabilities and Exposures project identifies the
following issues.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6814">CVE-2017-6814</a>

    <p>Cross-Site Scripting (XSS) vulnerability via media file metadata</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6815">CVE-2017-6815</a>

    <p>Control characters can trick redirect URL validation in
    wp-includes/pluggable.php</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6816">CVE-2017-6816</a>

    <p>Unintended files can be deleted by administrators using the plugin
    deletion functionality</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.6.1+dfsg-1~deb7u14.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-860.data"
# $Id: $
