<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in QEMU, a fast processor
emulator. The Common Vulnerabilities and Exposures project identifies
the following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5239">CVE-2015-5239</a>

   <p>Lian Yihan discovered that QEMU incorrectly handled certain payload
   messages in the VNC display driver. A malicious guest could use this
   issue to cause the QEMU process to hang, resulting in a denial of
   service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2857">CVE-2016-2857</a>

   <p>Ling Liu discovered that QEMU incorrectly handled IP checksum
   routines. An attacker inside the guest could use this issue to cause
   QEMU to crash, resulting in a denial of service, or possibly leak
   host memory bytes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4020">CVE-2016-4020</a>

   <p>Donghai Zdh discovered that QEMU incorrectly handled the Task
   Priority Register(TPR). A privileged attacker inside the guest could
   use this issue to possibly leak host memory bytes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4439">CVE-2016-4439</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2016-6351">CVE-2016-6351</a></p>

   <p>Li Qiang disovered that the emulation of the 53C9X Fast SCSI
   Controller is affected by out of bound access issues.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5403">CVE-2016-5403</a>

   <p>Zhenhao Hong discovered that a malicious guest administrator can
   cause unbounded memory allocation in QEMU (which can cause an
   Out-of-Memory condition) by submitting virtio requests without
   bothering to wait for completion.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.2+dfsg-6+deb7u14.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-573.data"
# $Id: $
