<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This security update fixes a number of security issues in Xen in wheezy.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.1.6.1-1+deb7u1.</p>

<p>We recommend that you upgrade your libidn packages.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-2752">CVE-2015-2752</a>

    <p>The XEN_DOMCTL_memory_mapping hypercall in Xen 3.2.x through 4.5.x,
    when using a PCI passthrough device, is not preemptable, which
    allows local x86 HVM domain users to cause a denial of service (host
    CPU consumption) via a crafted request to the device model
    (qemu-dm).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-2756">CVE-2015-2756</a>

    <p>QEMU, as used in Xen 3.3.x through 4.5.x, does not properly restrict
    access to PCI command registers, which might allow local HVM guest
    users to cause a denial of service (non-maskable interrupt and host
    crash) by disabling the (1) memory or (2) I/O decoding for a PCI
    Express device and then accessing the device, which triggers an
    Unsupported Request (UR) response.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5165">CVE-2015-5165</a>

    <p>The C+ mode offload emulation in the RTL8139 network card device
    model in QEMU, as used in Xen 4.5.x and earlier, allows remote
    attackers to read process heap memory via unspecified vectors.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5307">CVE-2015-5307</a>

    <p>The KVM subsystem in the Linux kernel through 4.2.6, and Xen 4.3.x
    through 4.6.x, allows guest OS users to cause a denial of service
    (host OS panic or hang) by triggering many #AC (aka Alignment Check)
    exceptions, related to svm.c and vmx.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7969">CVE-2015-7969</a>

    <p>Multiple memory leaks in Xen 4.0 through 4.6.x allow local guest
    administrators or domains with certain permission to cause a denial
    of service (memory consumption) via a large number of <q>teardowns</q> of
    domains with the vcpu pointer array allocated using the (1)
    XEN_DOMCTL_max_vcpus hypercall or the xenoprofile state vcpu pointer
    array allocated using the (2) XENOPROF_get_buffer or (3)
    XENOPROF_set_passive hypercall.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7970">CVE-2015-7970</a>

    <p>The p2m_pod_emergency_sweep function in arch/x86/mm/p2m-pod.c in Xen
    3.4.x, 3.5.x, and 3.6.x is not preemptible, which allows local x86
    HVM guest administrators to cause a denial of service (CPU
    consumption and possibly reboot) via crafted memory contents that
    triggers a "time-consuming linear scan," related to
    Populate-on-Demand.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7971">CVE-2015-7971</a>

    <p>Xen 3.2.x through 4.6.x does not limit the number of printk console
    messages when logging certain pmu and profiling hypercalls, which
    allows local guests to cause a denial of service via a sequence of
    crafted (1) HYPERCALL_xenoprof_op hypercalls, which are not properly
    handled in the do_xenoprof_op function in common/xenoprof.c, or (2)
    HYPERVISOR_xenpmu_op hypercalls, which are not properly handled in
    the do_xenpmu_op function in arch/x86/cpu/vpmu.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7972">CVE-2015-7972</a>

    <p>The (1) libxl_set_memory_target function in tools/libxl/libxl.c and
    (2) libxl__build_post function in tools/libxl/libxl_dom.c in Xen
    3.4.x through 4.6.x do not properly calculate the balloon size when
    using the populate-on-demand (PoD) system, which allows local HVM
    guest users to cause a denial of service (guest crash) via
    unspecified vectors related to "heavy memory pressure."</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8104">CVE-2015-8104</a>

    <p>The KVM subsystem in the Linux kernel through 4.2.6, and Xen 4.3.x
    through 4.6.x, allows guest OS users to cause a denial of service
    (host OS panic or hang) by triggering many #DB (aka Debug)
    exceptions, related to svm.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8339">CVE-2015-8339</a>

    <p>The memory_exchange function in common/memory.c in Xen 3.2.x through
    4.6.x does not properly hand back pages to a domain, which might
    allow guest OS administrators to cause a denial of service (host
    crash) via unspecified vectors related to domain teardown.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8340">CVE-2015-8340</a>

    <p>The memory_exchange function in common/memory.c in Xen 3.2.x through
    4.6.x does not properly release locks, which might allow guest OS
    administrators to cause a denial of service (deadlock or host crash)
    via unspecified vectors, related to XENMEM_exchange error handling.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8550">CVE-2015-8550</a>

    <p>Xen, when used on a system providing PV backends, allows local guest
    OS administrators to cause a denial of service (host OS crash) or
    gain privileges by writing to memory shared between the frontend and
    backend, aka a double fetch vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8554">CVE-2015-8554</a>

    <p>Buffer overflow in hw/pt-msi.c in Xen 4.6.x and earlier, when using
    the qemu-xen-traditional (aka qemu-dm) device model, allows local
    x86 HVM guest administrators to gain privileges by leveraging a
    system with access to a passed-through MSI-X capable physical PCI
    device and MSI-X table entries, related to a "write path."</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8555">CVE-2015-8555</a>

    <p>Xen 4.6.x, 4.5.x, 4.4.x, 4.3.x, and earlier do not initialize x86
    FPU stack and XMM registers when XSAVE/XRSTOR are not used to manage
    guest extended register state, which allows local guest domains to
    obtain sensitive information from other domains via unspecified
    vectors.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8615">CVE-2015-8615</a>

    <p>The hvm_set_callback_via function in arch/x86/hvm/irq.c in Xen 4.6
    does not limit the number of printk console messages when logging
    the new callback method, which allows local HVM guest OS users to
    cause a denial of service via a large number of changes to the
    callback method (HVM_PARAM_CALLBACK_IRQ).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1570">CVE-2016-1570</a>

    <p>The PV superpage functionality in arch/x86/mm.c in Xen 3.4.0, 3.4.1,
    and 4.1.x through 4.6.x allows local PV guests to obtain sensitive
    information, cause a denial of service, gain privileges, or have
    unspecified other impact via a crafted page identifier (MFN) to the
    (1) MMUEXT_MARK_SUPER or (2) MMUEXT_UNMARK_SUPER sub-op in the
    HYPERVISOR_mmuext_op hypercall or (3) unknown vectors related to
    page table updates.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1571">CVE-2016-1571</a>

    <p>The paging_invlpg function in include/asm-x86/paging.h in Xen 3.3.x
    through 4.6.x, when using shadow mode paging or nested
    virtualization is enabled, allows local HVM guest users to cause a
    denial of service (host crash) via a non-canonical guest address in
    an INVVPID instruction, which triggers a hypervisor bug check.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2270">CVE-2016-2270</a>

    <p>Xen 4.6.x and earlier allows local guest administrators to cause a
    denial of service (host reboot) via vectors related to multiple
    mappings of MMIO pages with different cachability settings.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2271">CVE-2016-2271</a>

    <p>VMX in Xen 4.6.x and earlier, when using an Intel or Cyrix CPU,
    allows local HVM guest users to cause a denial of service (guest
    crash) via vectors related to a non-canonical RIP.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-479.data"
# $Id: $
