Starting results calculation at Sun Oct  2 06:33:53 2022

Option 1 "Only one installer, including non-free firmware"
Option 2 "Recommend installer containing non-free firmware"
Option 3 "Allow presenting non-free installers alongside the free one"
Option 4 "Installer with non-free software is not part of Debian"
Option 5 "Change SC for non-free firmware in installer, one installer"
Option 6 "Change SC for non-free firmware in installer, keep both installers"
Option 7 "None of the above"

In the following table, tally[row x][col y] represents the votes that
option x received over option y.

                  Option
              1     2     3     4     5     6     7 
            ===   ===   ===   ===   ===   ===   === 
Option 1          158   206   270    72   129   264 
Option 2    170         235   286   121    75   291 
Option 3    144    99         294   127    84   306 
Option 4     80    64    53          74    55   135 
Option 5    229   219   229   279         169   289 
Option 6    216   253   266   298   163         311 
Option 7     91    63    51   197    63    42       



Looking at row 2, column 1, Recommend installer containing non-free firmware
received 170 votes over Only one installer, including non-free firmware

Looking at row 1, column 2, Only one installer, including non-free firmware
received 158 votes over Recommend installer containing non-free firmware.

Option 1 Reached quorum: 264 > 47.9765567751584
Option 2 Reached quorum: 291 > 47.9765567751584
Option 3 Reached quorum: 306 > 47.9765567751584
Option 4 Reached quorum: 135 > 47.9765567751584
Option 5 Reached quorum: 289 > 47.9765567751584
Option 6 Reached quorum: 311 > 47.9765567751584


Option 1 passes Majority.               2.901 (264/91) > 1
Option 2 passes Majority.               4.619 (291/63) > 1
Option 3 passes Majority.               6.000 (306/51) > 1
Dropping Option 4 because of Majority. (0.6852791878172588832487309644670050761421)  0.685 (135/197) <= 1
Option 5 passes Majority.               4.587 (289/63) >= 3
Option 6 passes Majority.               7.405 (311/42) >= 3


  Option 2 defeats Option 1 by ( 170 -  158) =   12 votes.
  Option 1 defeats Option 3 by ( 206 -  144) =   62 votes.
  Option 5 defeats Option 1 by ( 229 -   72) =  157 votes.
  Option 6 defeats Option 1 by ( 216 -  129) =   87 votes.
  Option 1 defeats Option 7 by ( 264 -   91) =  173 votes.
  Option 2 defeats Option 3 by ( 235 -   99) =  136 votes.
  Option 5 defeats Option 2 by ( 219 -  121) =   98 votes.
  Option 6 defeats Option 2 by ( 253 -   75) =  178 votes.
  Option 2 defeats Option 7 by ( 291 -   63) =  228 votes.
  Option 5 defeats Option 3 by ( 229 -  127) =  102 votes.
  Option 6 defeats Option 3 by ( 266 -   84) =  182 votes.
  Option 3 defeats Option 7 by ( 306 -   51) =  255 votes.
  Option 5 defeats Option 6 by ( 169 -  163) =    6 votes.
  Option 5 defeats Option 7 by ( 289 -   63) =  226 votes.
  Option 6 defeats Option 7 by ( 311 -   42) =  269 votes.


The Schwartz Set contains:
	 Option 5 "Change SC for non-free firmware in installer, one installer"



-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

The winners are:
	 Option 5 "Change SC for non-free firmware in installer, one installer"

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

