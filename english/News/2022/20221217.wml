<define-tag pagetitle>Updated Debian 11: 11.6 released</define-tag>
<define-tag release_date>2022-12-17</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>The Debian project is pleased to announce the sixth update of its
stable distribution Debian <release> (codename <q><codename></q>). 
This point release mainly adds corrections for security issues,
along with a few adjustments for serious problems.  Security advisories
have already been published separately and are referenced where available.</p>

<p>Please note that the point release does not constitute a new version of Debian
<release> but only updates some of the packages included.  There is
no need to throw away old <q><codename></q> media. After installation,
packages can be upgraded to the current versions using an up-to-date Debian
mirror.</p>

<p>Those who frequently install updates from security.debian.org won't have
to update many packages, and most such updates are
included in the point release.</p>

<p>New installation images will be available soon at the regular locations.</p>

<p>Upgrading an existing installation to this revision can be achieved by
pointing the package management system at one of Debian's many HTTP mirrors.
A comprehensive list of mirrors is available at:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Miscellaneous Bugfixes</h2>

<p>This stable update adds a few important corrections to the following packages:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction awstats "Fix cross site scripting issue [CVE-2022-46391]">
<correction base-files "Update /etc/debian_version for the 11.6 point release">
<correction binfmt-support "Run binfmt-support.service after systemd-binfmt.service">
<correction clickhouse "Fix out-of-bounds read issues [CVE-2021-42387 CVE-2021-42388], buffer overflow issues [CVE-2021-43304 CVE-2021-43305]">
<correction containerd "CRI plugin: Fix goroutine leak during Exec [CVE-2022-23471]">
<correction core-async-clojure "Fix build failures in test suite">
<correction dcfldd "Fix SHA1 output on big-endian architectures">
<correction debian-installer "Rebuild against proposed-updates; increase Linux kernel ABI to 5.10.0-20">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debmirror "Add non-free-firmware to the default section list">
<correction distro-info-data "Add Ubuntu 23.04, Lunar Lobster; update Debian ELTS end dates; correct Debian 8 (jessie) release date">
<correction dojo "Fix prototype pollution issue [CVE-2021-23450]">
<correction dovecot-fts-xapian "Generate dependency on dovecot ABI version in use during build">
<correction efitools "Fix intermittent build failure due to incorrect dependency in makefile">
<correction evolution "Move Google Contacts addressbooks to CalDAV since the Google Contacts API has been turned off">
<correction evolution-data-server "Move Google Contacts addressbooks to CalDAV since the Google Contacts API has been turned off; fix compatibility with Gmail OAuth changes">
<correction evolution-ews "Fix retrieval of user certificates belonging to contacts">
<correction g810-led "Control device access with uaccess instead of making everything world-writable [CVE-2022-46338]">
<correction glibc "Fix regression in wmemchr and wcslen on CPUs that have AVX2 but not BMI2 (e.g. Intel Haswell)">
<correction golang-github-go-chef-chef "Fix intermittent test failure">
<correction grub-efi-amd64-signed "Don't strip Xen binaries, so they work again; include fonts in the memdisk build for EFI images; fix bug in core file code so errors are handled better; bump Debian SBAT level to 4">
<correction grub-efi-arm64-signed "Don't strip Xen binaries, so they work again; include fonts in the memdisk build for EFI images; fix bug in core file code so errors are handled better; bump Debian SBAT level to 4">
<correction grub-efi-ia32-signed "Don't strip Xen binaries, so they work again; include fonts in the memdisk build for EFI images; fix bug in core file code so errors are handled better; bump Debian SBAT level to 4">
<correction grub2 "Don't strip Xen binaries, so they work again; include fonts in the memdisk build for EFI images; fix bug in core file code so errors are handled better; bump Debian SBAT level to 4">
<correction hydrapaper "Add missing dependeny on python3-pil">
<correction isoquery "Fix test failure caused by a French translation change in the iso-codes package">
<correction jtreg6 "New package, required to build newer openjdk-11 versions">
<correction lemonldap-ng "Improve session destroy propagation [CVE-2022-37186]">
<correction leptonlib "Fix divide-by-zero [CVE-2022-38266]">
<correction libapache2-mod-auth-mellon "Fix open redirect issue [CVE-2021-3639]">
<correction libbluray "Fix BD-J support with recent Oracle Java updates">
<correction libconfuse "Fix a heap-based buffer over-read in cfg_tilde_expand [CVE-2022-40320]">
<correction libdatetime-timezone-perl "Update included data">
<correction libtasn1-6 "Fix out-of-bounds read issue [CVE-2021-46848]">
<correction libvncserver "Fix memory leak [CVE-2020-29260]; support larger screen sizes">
<correction linux "New upstream stable release; increase ABI to 20; [rt] Update to 5.10.158-rt77">
<correction linux-signed-amd64 "New upstream stable release; increase ABI to 20; [rt] Update to 5.10.158-rt77">
<correction linux-signed-arm64 "New upstream stable release; increase ABI to 20; [rt] Update to 5.10.158-rt77">
<correction linux-signed-i386 "New upstream stable release; increase ABI to 20; [rt] Update to 5.10.158-rt77">
<correction mariadb-10.5 "New upstream stable release; security fixes [CVE-2018-25032 CVE-2021-46669 CVE-2022-27376 CVE-2022-27377 CVE-2022-27378 CVE-2022-27379 CVE-2022-27380 CVE-2022-27381 CVE-2022-27382 CVE-2022-27383 CVE-2022-27384 CVE-2022-27386 CVE-2022-27387 CVE-2022-27444 CVE-2022-27445 CVE-2022-27446 CVE-2022-27447 CVE-2022-27448 CVE-2022-27449 CVE-2022-27451 CVE-2022-27452 CVE-2022-27455 CVE-2022-27456 CVE-2022-27457 CVE-2022-27458 CVE-2022-32081 CVE-2022-32082 CVE-2022-32083 CVE-2022-32084 CVE-2022-32085 CVE-2022-32086 CVE-2022-32087 CVE-2022-32088 CVE-2022-32089 CVE-2022-32091]">
<correction mod-wsgi "Drop X-Client-IP header when it is not a trusted header [CVE-2022-2255]">
<correction mplayer "Fix several security issues [CVE-2022-38850 CVE-2022-38851 CVE-2022-38855 CVE-2022-38858 CVE-2022-38860 CVE-2022-38861 CVE-2022-38863 CVE-2022-38864 CVE-2022-38865 CVE-2022-38866]">
<correction mutt "Fix gpgme crash when listing keys in a public key block, and public key block listing for old versions of gpgme">
<correction nano "Fix crashes and a potential data loss issue">
<correction nftables "Fix off-by-one / double free error">
<correction node-hawk "Parse URLs using stdlib [CVE-2022-29167]">
<correction node-loader-utils "Fix prototype pollution issue [CVE-2022-37599 CVE-2022-37601], regular expression-based denial of service issue [CVE-2022-37603]">
<correction node-minimatch "Improve protection against regular expression-based denial of service [CVE-2022-3517]; fix regression in patch for CVE-2022-3517">
<correction node-qs "Fix prototype pollution issue [CVE-2022-24999]">
<correction node-xmldom "Fix prototype pollution issue [CVE-2022-37616]; prevent insertion of non-well-formed nodes [CVE-2022-39353]">
<correction nvidia-graphics-drivers "New upstream release; security fixes [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42255 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction nvidia-graphics-drivers-legacy-390xx "New upstream release; security fixes [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34680 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259]">
<correction nvidia-graphics-drivers-tesla-450 "New upstream release; security fixes [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction nvidia-graphics-drivers-tesla-470 "New upstream release; security fixes [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42255 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction omnievents "Add missing dependency on libjs-jquery to the omnievents-doc package">
<correction onionshare "Fix denial of service issue [CVE-2022-21689], HTML injection issue [CVE-2022-21690]">
<correction openvpn-auth-radius "Support verify-client-cert directive">
<correction postfix "New upstream stable release">
<correction postgresql-13 "New upstream stable release">
<correction powerline-gitstatus "Fix command injection via malicious repository config [CVE-2022-42906]">
<correction pysubnettree "Fix module build">
<correction speech-dispatcher "Reduce espeak buffer size to avoid synth artifacts">
<correction spf-engine "Fix pyspf-milter failing to start due to an invalid import statement">
<correction tinyexr "Fix heap overflow issues [CVE-2022-34300 CVE-2022-38529]">
<correction tinyxml "Fix infinite loop [CVE-2021-42260]">
<correction tzdata "Update data for Fiji, Mexico and Palestine; update leap seconds list">
<correction virglrenderer "Fix out-of-bounds write issue [CVE-2022-0135]">
<correction x2gothinclient "Make the x2gothinclient-minidesktop package provide the lightdm-greeter virtual package">
<correction xfig "Fix buffer overflow issue [CVE-2021-40241]">
</table>


<h2>Security Updates</h2>


<p>This revision adds the following security updates to the stable release.
The Security Team has already released an advisory for each of these
updates:</p>

<table border=0>
<tr><th>Advisory ID</th>  <th>Package</th></tr>
<dsa 2022 5212 chromium>
<dsa 2022 5223 chromium>
<dsa 2022 5224 poppler>
<dsa 2022 5225 chromium>
<dsa 2022 5226 pcs>
<dsa 2022 5227 libgoogle-gson-java>
<dsa 2022 5228 gdk-pixbuf>
<dsa 2022 5229 freecad>
<dsa 2022 5230 chromium>
<dsa 2022 5231 connman>
<dsa 2022 5232 tinygltf>
<dsa 2022 5233 e17>
<dsa 2022 5234 fish>
<dsa 2022 5235 bind9>
<dsa 2022 5236 expat>
<dsa 2022 5239 gdal>
<dsa 2022 5240 webkit2gtk>
<dsa 2022 5241 wpewebkit>
<dsa 2022 5242 maven-shared-utils>
<dsa 2022 5243 lighttpd>
<dsa 2022 5244 chromium>
<dsa 2022 5245 chromium>
<dsa 2022 5246 mediawiki>
<dsa 2022 5247 barbican>
<dsa 2022 5248 php-twig>
<dsa 2022 5249 strongswan>
<dsa 2022 5250 dbus>
<dsa 2022 5251 isc-dhcp>
<dsa 2022 5252 libreoffice>
<dsa 2022 5253 chromium>
<dsa 2022 5254 python-django>
<dsa 2022 5255 libksba>
<dsa 2022 5256 bcel>
<dsa 2022 5257 linux-signed-arm64>
<dsa 2022 5257 linux-signed-amd64>
<dsa 2022 5257 linux-signed-i386>
<dsa 2022 5257 linux>
<dsa 2022 5258 squid>
<dsa 2022 5260 lava>
<dsa 2022 5261 chromium>
<dsa 2022 5263 chromium>
<dsa 2022 5264 batik>
<dsa 2022 5265 tomcat9>
<dsa 2022 5266 expat>
<dsa 2022 5267 pysha3>
<dsa 2022 5268 ffmpeg>
<dsa 2022 5269 pypy3>
<dsa 2022 5270 ntfs-3g>
<dsa 2022 5271 libxml2>
<dsa 2022 5272 xen>
<dsa 2022 5273 webkit2gtk>
<dsa 2022 5274 wpewebkit>
<dsa 2022 5275 chromium>
<dsa 2022 5276 pixman>
<dsa 2022 5277 php7.4>
<dsa 2022 5278 xorg-server>
<dsa 2022 5279 wordpress>
<dsa 2022 5280 grub-efi-amd64-signed>
<dsa 2022 5280 grub-efi-arm64-signed>
<dsa 2022 5280 grub-efi-ia32-signed>
<dsa 2022 5280 grub2>
<dsa 2022 5281 nginx>
<dsa 2022 5283 jackson-databind>
<dsa 2022 5285 asterisk>
<dsa 2022 5286 krb5>
<dsa 2022 5287 heimdal>
<dsa 2022 5288 graphicsmagick>
<dsa 2022 5289 chromium>
<dsa 2022 5290 commons-configuration2>
<dsa 2022 5291 mujs>
<dsa 2022 5292 snapd>
<dsa 2022 5293 chromium>
<dsa 2022 5294 jhead>
<dsa 2022 5295 chromium>
<dsa 2022 5296 xfce4-settings>
<dsa 2022 5297 vlc>
<dsa 2022 5298 cacti>
<dsa 2022 5299 openexr>
</table>



<h2>Debian Installer</h2>
<p>The installer has been updated to include the fixes incorporated
into stable by the point release.</p>

<h2>URLs</h2>

<p>The complete lists of packages that have changed with this revision:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>The current stable distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Proposed updates to the stable distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>stable distribution information (release notes, errata etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Security announcements and information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely
free operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a>, send mail to
&lt;press@debian.org&gt;, or contact the stable release team at
&lt;debian-release@lists.debian.org&gt;.</p>

