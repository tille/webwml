<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Felix Wilhelm of Google Project Zero discovered a flaw in git, a fast,
scalable, distributed revision control system. With a crafted URL that
contains a newline, the credential helper machinery can be fooled to
return credential information for a wrong host.</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 1:2.11.0-3+deb9u6.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1:2.20.1-2+deb10u2.</p>

<p>We recommend that you upgrade your git packages.</p>

<p>For the detailed security status of git please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/git">https://security-tracker.debian.org/tracker/git</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4657.data"
# $Id: $
