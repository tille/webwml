<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Seokchan Yoon discovered that missing sanitising in the email and URL
validators of Django, a Python web development framework, could result
in denial of service.</p>

<p>For the oldstable distribution (bullseye), this problem has been fixed
in version 2:2.2.28-1~deb11u2. This update also addresses
<a href="https://security-tracker.debian.org/tracker/CVE-2023-23969">CVE-2023-23969</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2023-31047">CVE-2023-31047</a>
and <a href="https://security-tracker.debian.org/tracker/CVE-2023-24580">CVE-2023-24580</a>.</p>

<p>For the stable distribution (bookworm), this problem has been fixed in
version 3:3.2.19-1+deb12u1.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>For the detailed security status of python-django please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-django">\
https://security-tracker.debian.org/tracker/python-django</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5465.data"
# $Id: $
