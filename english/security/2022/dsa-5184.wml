<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in the Xen hypervisor, which
could result in privilege escalation. In addition this updates provides
mitigations for the <q>Retbleed</q> speculative execution attack and the
<q>MMIO stale data</q> vulnerabilities.</p>

<p>For additional information please refer to the following pages:
<a href="https://xenbits.xen.org/xsa/advisory-404.html">https://xenbits.xen.org/xsa/advisory-404.html</a>
<a href="https://xenbits.xen.org/xsa/advisory-407.html">https://xenbits.xen.org/xsa/advisory-407.html</a></p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 4.14.5+24-g87d90d511c-1.</p>

<p>We recommend that you upgrade your xen packages.</p>

<p>For the detailed security status of xen please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/xen">\
https://security-tracker.debian.org/tracker/xen</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5184.data"
# $Id: $
