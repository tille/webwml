<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Alexander Lakhin discovered that the autovacuum feature and multiple
commands could escape the "security-restricted operation" sandbox.</p>

<p>For additional information please refer to the upstream announcement
at <a href="https://www.postgresql.org/support/security/CVE-2022-1552/">\
https://www.postgresql.org/support/security/CVE-2022-1552/</a>/</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 13.7-0+deb11u1.</p>

<p>We recommend that you upgrade your postgresql-13 packages.</p>

<p>For the detailed security status of postgresql-13 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/postgresql-13">\
https://security-tracker.debian.org/tracker/postgresql-13</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5136.data"
# $Id: $
