#use wml::debian::template title="Informações sobre segurança" GEN_TIME="yes"
#use wml::debian::toc
#use wml::debian::recent_list_security
#use wml::debian::translation-check translation="e49cfbc5f5d8df40e0852a5da747d32544d67b06"
#include "$(ENGLISHDIR)/releases/info"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Mantendo seu sistema Debian seguro</a></li>
<li><a href="#DSAS">Alertas recentes</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> O Debian leva a segurança
muito a sério. Lidamos com todos os problemas de segurança trazidos ao nosso
conhecimento e garantimos que sejam corrigidos dentro de um prazo razoável.</p>
</aside>

<p>
A experiência mostrou que a <q>segurança através da obscuridade</q> nunca
funciona. Portanto, a divulgação pública permite soluções mais rápidas e
melhores dos problemas de segurança. A esse respeito, esta página aborda o
status do Debian em relação a várias falhas de segurança conhecidas, que podem
afetar potencialmente o sistema operacional Debian.
</p>

<p>
O projeto Debian coordena muitos alertas de segurança com outros(as)
fornecedores(as) de software livre e, como resultado, esses alertas são
publicados no mesmo dia em que uma vulnerabilidade é tornada pública.
</p>

# "reasonable timeframe" might be too vague, but we don't have 
# accurate statistics. For older (out of date) information and data
# please read:
# https://www.debian.org/News/2004/20040406  [ Year 2004 data ]
# and (older)
# https://people.debian.org/~jfs/debconf3/security/ [ Year 2003 data ]
# https://lists.debian.org/debian-security/2001/12/msg00257.html [ Year 2001]
# If anyone wants to do up-to-date analysis please contact me (jfs)
# and I will provide scripts, data and database schemas.

<p>
O Debian também participa dos esforços de padronização de segurança:
</p>

<ul>
   <li>Os <a href="#DSAS">alertas de segurança Debian</a> são
<a href="cve-compatibility">compatíveis com o CVE</a> (reveja as
<a href="crossreferences">referências cruzadas</a>).</li>
   <li>O Debian está representado no conselho do projeto
<a href="https://oval.cisecurity.org/">Open Vulnerability Assessment Language</a>.</li>
</ul>

<h2><a id="keeping-secure">Mantendo seu sistema Debian seguro</a></h2>

<p>
Para receber os últimos alertas de segurança Debian, por favor inscreva-se na
lista de discussão
<a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.

<p>
Além disso, você pode usar o
<a href="https://packages.debian.org/stable/admin/apt">APT</a> para obter
facilmente as atualizações de segurança mais recentes. Para manter seu
sistema operacional Debian atualizado com os patches de segurança, por favor
adicione a seguinte linha ao seu arquivo <code>/etc/apt/sources.list</code>:
</p>

<pre>
deb http://security.debian.org/debian-security <current_release_security_name> main contrib non-free non-free-firmware
</pre>

<p>
Depois de salvar as alterações, execute os dois comandos a seguir para baixar e
instalar as atualizações pendentes:
</p>

<pre>
apt-get update &amp;&amp; apt-get upgrade
</pre> 

<p>
O repositório de segurança é assinado com as habituais
<a href="https://ftp-master.debian.org/keys.html">chaves de repositório Debian</a>.
</p>

<p>
Para mais informações sobre problemas de segurança no Debian, por favor
consulte nossa FAQ e nossa documentação:
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="faq">FAQ sobre segurança</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="../doc/user-manuals#securing">protegendo o Debian</a></button></p>

<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">Alertas recentes</a></h2>

<p>Estas páginas web incluem um arquivo condensado de alertas de segurança
postados na <a href="https://lists.debian.org/debian-security-announce/">lista debian-security-announce</a>.<br>
<a href="dsa.html#DSAS">Novo formato de lista</a>

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (titles only)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (summaries)" href="dsa-long">
:#rss#}

<p>
Os últimos alertas de segurança do Debian estão disponíveis como
<a href="dsa">arquivos RDF</a>. Também oferecemos uma
<a href="dsa-long">versão um pouco mais longa</a> dos arquivos, que inclui o
primeiro parágrafo do alerta correspondente. Dessa forma, você pode identificar
facilmente do que se trata o alerta.
</p>

#include "$(ENGLISHDIR)/security/index.include"
<p>Os alertas de segurança antigos também estão disponíveis:
<:= get_past_sec_list(); :>

<p>As versões Debian não são vulneráveis a todos os problemas de segurança.
O <a href="https://security-tracker.debian.org/">rastreador de segurança
Debian</a> coleta todas as informações sobre o estado de vulnerabilidade dos
pacotes Debian e pode ser pesquisado por nome CVE ou por pacote.</p>
