#use wml::debian::translation-check translation="925ae76facb2d1bc28343b8f7956afe7f13c0a12"
<define-tag pagetitle>DebConf22 in Prizren gesloten en data van DebConf23 aangekondigd</define-tag>

<define-tag release_date>2022-07-24</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

<p>
Vandaag, zondag 24 juli 2022, liep de jaarlijkse conferentie van ontwikkelaars en medewerkers van Debian ten einde.
Met 260 deelnemers uit 38 verschillende landen en in totaal 91 lezingen, discussiebijeenkomsten, informele uitwisselingen (Birds of a Feather - BoF), werkgroepen en activiteiten was <a href="https://debconf22.debconf.org">DebConf22</a> een groot succes.
</p>

<p>
De conferentie werd voorafgegaan door het jaarlijkse DebCamp, dat van 10 tot 16 juli werd gehouden en gericht was op individueel werk en teamsprints met persoonlijke onderlinge samenwerking voor het verder ontwikkelen van Debian.
In het bijzonder zijn er dit jaar sprints geweest om de ontwikkeling vooruit te helpen van Mobian/Debian op de mobilofoon, van reproduceerbare compilaties en van Python in Debian, en er was een BootCamp voor nieuwkomers, om hen kennis te laten maken met Debian en hen wat praktijkervaring te laten opdoen met het gebruik ervan en met het leveren van bijdragen aan de gemeenschap.
</p>

<p>
De eigenlijke conferentie van ontwikkelaars van Debian ging van start op zondag 17 juli 2022.
Samen met activiteiten zoals de traditionele lezing van de projectleider (Bits from the DPL), de permanente ondertekening van sleutels, de blitzlezingen  (lightning talks) en de aankondiging van de DebConf van volgend jaar
(<a href="https://wiki.debian.org/DebConf/23">DebConf23</a> in Kochi, India),
waren er verschillende sessies van de teams van programmeertalen zoals Python, Perl en Ruby, alsook nieuwsupdates over verschillende projecten en interne Debian-teams, discussiesessies (BoF's) van vele technische teams
(langetermijnondersteuning, Android-hulpmiddelen, derivaten van Debian, het team voor het installatiesysteem en de installatie-images van Debian, Debian voor wetenschappelijk gebruik...) en lokale gemeenschappen (Debian Brazilië, Debian India, de lokale teams van Debian),
samen met vele andere interessante evenementen met betrekking tot Debian en vrije software.
</p>

<p>
De <a href="https://debconf22.debconf.org/schedule/">agenda</a> werd elke dag bijgewerkt met geplande en ad-hocactiviteiten die door de deelnemers in de loop van de hele conferentie werden voorgesteld.
Verschillende activiteiten die de voorbije jaren niet konden worden georganiseerd wegens de covid-pandemie kwamen terug op het programma van de conferentie te staan: een jobbeurs, een open-mic en poëzieavond, de traditionele kaas- en wijnavond, de groepsfoto's en de daguitstap.
</p>

<p>
Voor wie er niet bij kon zijn, werden de meeste lezingen en sessies opgenomen in functie van de livestreams en werden de video's beschikbaar gesteld via de
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2022/DebConf22/">website van het archief van Debian-bijeenkomsten</a>.
Bijna voor alle sessies was deelname op afstand mogelijk via IRC-berichtenapps of online collaboratieve tekstdocumenten.
</p>

<p>
De <a href="https://debconf22.debconf.org/">website van DebConf22</a>
blijft actief voor archiefdoeleinden en zal links blijven aanbieden naar de presentaties en video's van lezingen en evenementen.</p>

<p>
Volgend jaar wordt <a href="https://wiki.debian.org/DebConf/23">DebConf23</a> gehouden in Kochi, India, van 10 september tot 16 september 2023.
Traditiegetrouw zullen de lokale organisatoren in India, voorafgaand aan de volgende DebConf, de conferentieactiviteiten starten met DebCamp (03 tot 09 september 2023), met bijzondere aandacht voor individueel en teamwerk ter verbetering van de distributie.
</p>

<p>
DebConf zet zich in om een veilige en gastvrije omgeving te zijn voor alle deelnemers.
Zie de <a href="https://debconf22.debconf.org/about/coc/">webpagina over de gedragscode op de website van DebConf22</a>
voor meer informatie hierover.
</p>

<p>
Debian dankt de talrijke <a href="https://debconf22.debconf.org/sponsors/">sponsors</a> voor hun engagement bij het ondersteunen van DebConf22,
in het bijzonder onze platina sponsors:
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://itp-prizren.com/">ITP Prizren</a>
en <a href="https://google.com/">Google</a>

</p>

<h2>Over Debian</h2>

<p>
Het Debian-project werd in 1993 door Ian Murdock opgericht als een echt vrij gemeenschapsproject. Sindsdien is het project uitgegroeid tot een van de grootste en meest invloedrijke opensourceprojecten. Duizenden vrijwilligers van over de hele wereld werken samen om Debian-software te maken en te onderhouden. Debian is beschikbaar in 70 talen en ondersteunt een groot aantal computertypes. Het noemt zichzelf het <q>universele besturingssysteem</q>.
</p>

<h2>Over DebConf</h2>

<p>
DebConf is de conferentie van de ontwikkelaars van het Debian-project. Naast een volledig programma van technische, sociale en beleidslezingen, biedt DebConf een kans voor ontwikkelaars, medewerkers en andere geïnteresseerden om elkaar persoonlijk te ontmoeten en nauwer samen te werken. Het heeft sinds 2000 jaarlijks plaatsgevonden op uiteenlopende locaties als Schotland, Argentinië en Bosnië en Herzegovina. Meer informatie over DebConf is te vinden op
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>Over Lenovo</h2>
<p>
Als wereldwijde technologieleider die een breed gamma geconnecteerde producten fabriceert, waaronder smartphones, tablets, pc's en werkstations, evenals AR/VR-apparaten, en slimme oplossingen voor thuis en op kantoor alsook datacentertoepassingen aanbiedt, begrijpt <a href="https://www.lenovo.com">Lenovo</a> hoe essentieel open systemen en platforms zijn in een geconnecteerde wereld.
</p>

<h2>Over Infomaniak</h2>
<p>
<a href="https://www.infomaniak.com">Infomaniak</a>
is het grootste webhostingbedrijf van Zwitserland en het biedt ook back-up- en opslagdiensten, oplossingen voor organisatoren van evenementen, livestreaming en video-on-demanddiensten. Het is de volledige eigenaar van zijn datacenters en van alle elementen die van cruciaal belang zijn voor de werking van de door het bedrijf aangeboden diensten en producten (zowel software als hardware).
</p>

<h2>Over ITP Prizren</h2>
<p>
<a href="https://itp-prizren.com/">Innovatie- en TrainingPark Prizren</a>
wil een veranderend en stimulerend element zijn op het gebied van ICT, de agro- en voedingsnijverheid en de creatieve industrie, door het creëren en beheren van een gunstig klimaat en efficiënte diensten voor kmo's, waarbij verschillende soorten innovaties worden benut die Kosovo kunnen helpen zijn ontwikkelingsniveau op het gebied van industrie en onderzoek te verbeteren, wat de economie en de samenleving van het land als geheel ten goede komt.
</p>

<h2>Over Google</h2>
<p>
<a href="https://google.com/">Google</a>
is een van de grootste technologiebedrijven ter wereld, dat een breed gamma aan internetgerelateerde diensten en producten aanbiedt, zoals online advertentietechnologieën, zoekfuncties, clouddiensten, software en hardware.
</p>
<p>
Google ondersteunt Debian al meer dan tien jaar door DebConf te sponsoren, en het is ook een Debian-partner die delen sponsort van de continue integratie-infrastructuur van <a href="https://salsa.debian.org">Salsa</a> binnen het cloudplatform van Google.

</p>

<h2>Contactinformatie</h2>

<p>Raadpleeg voor verdere informatie de webpagina van DebConf22 op
<a href="https://debconf22.debconf.org/">https://debconf22.debconf.org/</a>
of stuur een e-mail naar &lt;press@debian.org&gt;.</p>
