#use wml::debian::template title="Veelgestelde vragen over patentbeleid bij gemeenschapsdistributies" BARETITLE="true"
#use wml::debian::translation-check translation="ba01cfdc529712e3626bdf15fd37d39e94126794"

<pre><code>Versie: 1.0
Gepubliceerd: 8 juli 2011
</code></pre>

<h1>Inleiding</h1>

<h2>Voor wie is dit document bedoeld?</h2>

<p>Dit document geeft informatie over patenten en patentaansprakelijkheid welke
nuttig is voor ontwikkelaars die werken aan gemeenschapsdistributies van Vrije
en Openbronsoftware (Free and Open Source Software - FOSS). Met
gemeenschapsdistributies bedoelen we verzamelingen van vrije softwarepakketten
die onderhouden en gedistribueerd worden door organisaties bestaande uit
vrijwilligers, waarbij noch de organisatie noch de vrijwilligers winst
nastreven met de activiteit. Dergelijke gemeenschapsdistributies kunnen hun
werk zowel verkopen als weggeven, mogelijk op cd's of USB-opslagmedia of door
betaalde downloads of gratis distributie.</p>

<p>Dit document is opgesteld door advocaten van het
<a href="http://www.softwarefreedom.org">Software Freedom Law Center</a> (SFLC)
op verzoek van het <a href="https://www.debian.org/">Debian-project</a> en kan
nuttig zijn voor soortgelijke FOSS-gemeenschapsdistributies. De verklaringen
over juridische zaken die dit document bevat, zijn op de datum van
samenstelling van dit document accuraat met betrekking tot de Amerikaanse
wetgeving en kunnen van toepassing zijn op andere rechtssystemen. Dit document
vormt echter geen juridisch advies. Het is niet gebaseerd op een analyse van
een specifieke feitelijke situatie, en elke advocaat die een mening geeft over
de vragen die hieronder worden gepresenteerd, zou zich moeten vergewissen van
de specifieke feiten en omstandigheden die deze informatie in een specifieke
 situatie zouden kunnen veranderen. U mag niet op dit document vertrouwen om
beslissingen te nemen die van invloed zijn op de wettelijke rechten of
verantwoordelijkheden van uw project in een reële situatie zonder SFLC of
andere advocaten te raadplegen.</p>

<h1>Achtergrondinformatie over patenten</h1>

<h2>Wat is een patent?</h2>

<p>Een patent is een door de staat verleend monopolie dat een uitvinder het
exclusieve recht verleent om de geclaimde uitvinding te maken, te verkopen, te
koop aan te bieden, te laten maken of te importeren voor de beperkte duur van
het patent. De patenthouder kan vervolgens, op exclusieve of niet-exclusieve
basis, een of meer van de verleende rechten in licentie geven.</p>

<h2>Hoe lang is de looptijd van een patent?</h2>

<p>Over het algemeen verlopen patenten die in de afgelopen 15 jaar door de
meeste regeringen zijn uitgegeven, 20 jaar vanaf de indieningsdatum van de
patentaanvraag. Amerikaanse patenten met een indieningsdatum vóór 8 juni 1995
bieden bescherming voor maximaal 17 jaar vanaf de datum van toekenning, of 20
jaar vanaf de indieningsdatum, afhankelijk van welke datum later valt.</p>

<p>Er bestaan uitzonderingen. Patenten kunnen verlengd worden door het
uitgiftebureau of door een rechtbank, maar dit gebeurt zelden voor patenten op
software. Patenttermijnen kunnen ook worden verkort in overleg met de aanvrager
tijdens de <q>berechting</q>, dat wil zeggen tijdens de procedure van het
octrooibureau welke tot uitgifte leidt. Als de looptijd van een patent tijdens
de patentverlening is ingekort, verschijnt er een verklaring van <q>terminal
disclaimer</q> (einddatumvoorbehoud) op de voorpagina van het patent.</p>

<h2>Waarin verschilt patentbescherming van auteursrechtelijke bescherming?</h2>

<p>Een eigenaar van een auteursrecht heeft het recht om te voorkomen dat anderen ongeautoriseerde kopieën van het auteursrechtelijk beschermde programma maken, maar niet om anderen te beletten zelfstandig een programma met dezelfde functies te maken. Onafhankelijke creatie is daarom een volledige bescherming tegen een beschuldiging van inbreuk op het auteursrecht. Bovendien vormt <q>redelijk gebruik</q> een bescherming tegen een beschuldiging van inbreuk op het auteursrecht of een substantiële inperking van het auteursrecht in elk auteursrechtensysteem. Het patentrecht kent geen vrijstelling voor redelijk gebruik en dus vormen onafhankelijke creatie, gebruik voor onderzoek of decompilatie en nabouwen met het oog op interoperabiliteit of voor educatieve doeleinden geen bescherming tegen een beschuldiging van inbreuk tegen het patent.</p>

<h2>Bestaat er een wereldwijd patent op iets?</h2>

<p>Momenteel bestaan er geen wereldwijde patenten.  Buiten de Europese Unie, waar aanvragen kunnen worden geconsolideerd, moeten patenten over het algemeen worden aangevraagd in elk land waar patentbescherming wordt nagestreefd.</p>

<h2>Wat zijn patentconclusies?</h2>

<p>De patentconclusies, die het belangrijkste deel van het patent vormen, bepalen de werkelijke reikwijdte van de uitvinding waarop het patent van toepassing is. Alleen de patentconclusies bepalen wat de exclusieve rechten dekken, dat wil zeggen dat het toepassen van de patentconclusies zonder licentie een inbreuk is. Het lezen en begrijpen van de conclusies van een patent is de sleutel tot het bepalen of een bepaald product of proces een inbreuk vormt.</p>

<p>Elke patentconclusie bestaat uit één zin. Een patentconclusie begint met een <q>preambule</q> gevolgd door een of meer <q>beperkingen</q>.</p>

<p>Software maakt alleen inbreuk op een patent als de software (of het systeem met ingebedde software) alles implementeert wat in een van de patentconclusies wordt genoemd. Als u een of meer elementen van een patentconclusie niet implementeert, dan kunt u niet rechtstreeks inbreuk maken op die patentconclusie.</p>

<h2>Wat is een onafhankelijke patentconclusie?</h2>

<p>Een patentconclusie wordt <q>onafhankelijk</q> genoemd, als deze niet verwijst naar een andere conclusie in het patent.</p>

<h2>Wat is een afhankelijke patentconclusie?</h2>

<p>Afhankelijke patentconclusies nemen expliciet de inhoud van andere conclusies in het patent op. Een afhankelijke conclusie is noodzakelijkerwijs beperkter in reikwijdte dan de conclusie waarvan ze afhangt, omdat ze één of meer bijkomende beperkingen bevat. In termen van een Venn-diagram is het dekkingsgebied van een afhankelijke patentconclusie volledig vervat binnen het dekkingsgebied van de patentconclusie waarnaar ze verwijst.</p>

<h2>Hoe worden software-gerelateerde patentconclusies geschreven??</h2>

<p>Software-gerelateerde patentconclusies in recent uitgegeven patenten hebben vaak de vorm van <q>systeem</q>- of <q>apparaat</q>conclusies, <q>methode</q>conclusies en <q>computerprogrammaproduct</q>- of <q>computerleesbaar medium</q>conclusies. Systeemconclusies verwijzen naar de elementen van een systeem (dat één of meer computers kan omvatten) als een soort machine of statisch object. Methodeconclusies hebben een algoritmische vorm. Computer-leesbare mediumconclusies dupliceren meestal de beperkingen die gevonden worden in overeenkomstige systeem- of methodeconclusies in het patent, maar zijn bedoeld om software te dekken die is opgenomen in een opslag- of distributiemedium. Computer-leesbare mediumconclusies worden ook vaak gebruikt bij aanspraken op uitvindingen die zich richten op gegevensstructuren en gebruikersinterfaces.
</p>

<h1>Inbreuk maken op een patent</h1>

<h2>Wat betekent <q>patentaansprakelijkheid</q>?</h2>

<p>Aansprakelijkheid is een wettelijke verantwoordelijkheid die door een rechtbank kan worden afgedwongen. In dit document gebruiken we de term <q>patentaansprakelijkheid</q> voor bevelen die een rechtbank kan geven als wordt vastgesteld dat een partij inbreuk pleegt op een patent. Als bijvoorbeeld wordt vastgesteld dat een partij inbreuk maakt, kan een rechtbank die partij gelasten om geld te betalen aan de patenthouder, <q>schadevergoeding</q> genaamd, en/of een bevel om het inbreukmakende gedrag te stoppen, wat een <q>gerechtelijk bevel</q> wordt genoemd.</p>

<h2>Wat betekent het om <q>inbreuk te maken</q> op een patent?</h2>

<p>Inbreuk maken op een patent betekent een of meer van de conclusies zonder licentie toepassen. Als iemand software gebruikt, maakt, verkoopt, laat maken, te koop aanbiedt of importeert die elk element uit een conclusie in een patent toepast, dan wordt dat patent geschonden door de software.</p>

<p>Het is mogelijk om aansprakelijk te zijn voor inbreuk zonder direct inbreuk te plegen. <q>Bijdragen aan</q> of <q>aanzetten tot</q> inbreuk kan ook leiden tot patentaansprakelijkheid.</p>

<h2>Wat is aanzetten tot inbreuk?</h2>

<p><q>Aanzetten tot inbreuk</q> betekent iemand anders actief aanmoedigen om inbreuk te maken op een octrooi. Aansprakelijkheid vereist het bewijs dat de aangeklaagde partij de intentie had om een derde inbreuk te laten plegen. Bovendien moet de aanstichter ofwel weten dat het patent bestaat, ofwel het bestaan ervan sterk vermoeden en moeite doen om het niet te weten. Als er bijvoorbeeld documentatie wordt geschreven door iemand die op de hoogte is van de patentconclusies, en die documentatie legt uit hoe het programma op een inbreukmakende manier moet worden gebruikt, kunnen de instructies worden beschouwd als aanzetting tot inbreuk. Wanneer een gemeenschap van vrijwilligers een softwarepakket en bijbehorende documentatie onderhoudt, kunnen ze niet aanzetten tot inbreuk, tenzij de vrijwilligers die de documentatie produceren op de hoogte zijn van het patent waarop vermoedelijk inbreuk wordt gemaakt.</p>

<h2>Wat is medeplichtigheid aan inbreuk?</h2>

<p><q>Medeplichtigheid aan inbreuk</q> betekent het verlenen van materiële hulp aan de inbreuk op een patent. In de context van software zou dit betekenen dat niet-inbreukmakende software wordt geleverd die kan worden gecombineerd met andere software of hardware om een inbreukmakend systeem te maken. Medeplichtigheid aan inbreuk vereist ook kennis van het patent waarop inbreuk wordt gemaakt. Bovendien, als de software substantiële niet-inbreukmakende toepassingen kent, is het geen medeplichtigheid aan inbreuk om het te verstrekken, zelfs als het vervolgens wordt gebruikt in een inbreukmakende combinatie.</p>

<h2>Wat zijn de gevolgen van het schenden van een patent?</h2>

<p>Als blijkt dat een partij inbreuk maakt op een octrooi, kunnen rechtbanken de stopzetting van het inbreukmakende gedrag, de betaling van schadevergoeding voor in het verleden gepleegde inbreuk of beide bevelen.  In dit document gebruiken we de term <q>patentaansprakelijkheid</q> om al deze gevolgen te omvatten.</p>

<h2>Wat is een gerechtelijk bevel?</h2>

<p>Een gerechtelijk bevel is een bevel van een rechtbank aan een persoon of personen om iets te doen of na te laten. Het schenden van een gerechtelijk bevel leidt ertoe dat men geacht wordt de rechtbank te minachten. Rechterlijke bevelen kunnen <q>voorlopig</q> zijn, om te voorkomen dat de toestand verandert terwijl het proces loopt, of <q>permanent</q>, om gedrag te bevelen of te verbieden als genoegdoening aan het einde van een rechtszaak, wanneer de aansprakelijkheid is vastgesteld. Een voorlopig rechterlijk bevel om inbreukmakend gedrag tijdens een proces te voorkomen, kan worden uitgevaardigd als de rechtbank oordeelt dat schadevergoeding aan het einde van de zaak niet zou volstaan om de rechten van de patenthouder te beschermen, als succes in de zaak waarschijnlijk wordt geacht en als het algemeen belang niet zou worden geschaad door het rechterlijk bevel. Een permanent rechterlijk bevel ter voorkoming van inbreukmakend gedrag kan het gevolg zijn van een bevinding dat iemand aansprakelijk is voor inbreuk.</p>

<h2>Kunnen er rechterlijke bevelen worden uitgevaardigd tegen FOSS-distributies?</h2>

<p>Ja. Als blijkt dat een FOSS-distributie inbreuk maakt op iemands geldige patent, kan er heel goed een permanent rechterlijk bevel komen tegen de verdere distributie van het inbreukmakende programma of de inbreukmakende functie.</p>

<p>Het is echter niet waarschijnlijk dat zo'n gerechtelijk bevel de verspreiding van de hele distributie, of zelfs van een heel pakket, zou verhinderen. Het is waarschijnlijker dat een functie of een reeks functies moet worden uitgeschakeld of aangepast zodat de software geen inbreuk meer maakt, of dat deze helemaal verwijderd moeten worden in het land waar de patentschending is vastgesteld.</p>

<p>Verder kan een ontwerp dat om de patentconclusies in kwestie heen gaat, voorkomen dat zelfs een functie of een reeks functies moeten worden verwijderd. Zodra zelfs maar één element van een patentconclusie niet langer wordt uitgeoefend, wordt de patentconclusie, zoals we al zeiden, niet langer geschonden. In patentgeschillen in de VS vindt het cruciale moment van vaststelling plaats in wat een <q><em>Markman</em>-hoorzitting</q> wordt genoemd, waarna de rechtbank een definitieve uitspraak doet over wat de patentconclusies in kwestie betekenen voor de inhoud van die rechtszaak. Als er eenmaal een <em>Markman</em>-hoorzitting heeft plaatsgevonden en de reikwijdte van de beweerde conclusies nauwgezet en onomstotelijk is gedefinieerd, wordt het veel gemakkelijker om eromheen te ontwerpen.</p>

<h2>Wat is schadevergoeding?</h2>

<p>In het patentrecht is schadevergoeding geld dat door de rechtbank aan de eiser wordt toegekend wanneer de gedaagde aansprakelijk is bevonden voor patentinbreuk. De wet kent geen maximale schadevergoeding toe voor patentinbreuk, maar wel een minimum - de redelijke licentierechten voor het gebruik van de uitvinding door de inbreukmaker. Daarnaast kan de rechtbank de schadevergoeding verhogen, tot drie keer de werkelijke schade, in gevallen van moedwillige inbreuk.</p>

<h2>Wat is een moedwillige inbreuk?</h2>

<p>Een inbreuk is moedwillig of opzettelijk als de inbreukmaker op de hoogte was van het patent, tenzij de inbreukmaker te goeder trouw geloofde dat het patent ongeldig was of dat zijn gedrag geen inbreuk maakte. De patenthouder moet alle elementen van de opzettelijkheid aantonen en in de VS moeten rechtbanken dit doen met een hogere bewijsstandaard, die <q>duidelijk en overtuigend bewijs</q> wordt genoemd.</p>

<h2>Ik had geen voorkennis van een patent, kan ik dan toch aansprakelijk worden gesteld?</h2>

<p>Kennis van een patent is in het algemeen niet vereist als de partij directe inbreuk wordt verweten. Om aansprakelijk te worden gesteld voor het aanzetten tot of bijdragen aan inbreuk is een vereiste, zoals we hebben gezegd, kennis hebben van het patent of specifieke inspanningen doen om te voorkomen dat men van het patent op de hoogte zou zijn.</p>

<p>In de praktijk vragen patenthouders meestal aan degenen die ze ervan verdenken een inbreuk plegen, om een licentie te nemen.  Als de partij de aangeboden licentie aanneemt, krijgt de houder licentierechten zonder dat hij ervoor hoeft te procederen. Als de partij de licentie afwijst, heeft de patenthouder deze op de hoogte gesteld en kan hij dus aanspraak maken op opzettelijke inbreuk, wat resulteert in een hogere schadevergoeding en de mogelijkheid om advocaatkosten te verhalen. Het is waarschijnlijk, maar niet zeker, dat voordat een gemeenschapsdistributie wordt aangeklaagd wegens patentinbreuk, deze minstens één brief zal ontvangen waarin wordt geëist dat een licentie wordt genomen.</p>

<h2>Wat als de inbreuk per ongeluk, onopzettelijk en onbedoeld was?</h2>

<p>Een onopzettelijke of onbedoelde inbreuk kan niet kwaadwillig zijn, zoals we hierboven hebben gezegd. Evenmin kan iemand per ongeluk bijdragen aan of aanzetten tot een inbreuk, aangezien zowel kennis als opzet vereist zijn. Maar iemand kan aansprakelijk zijn voor directe inbreuk, zonder kennis of opzet, door zonder meer inbreukmakende software te gebruiken of te verkopen of te maken of te laten maken.</p>

<h2>Hoe kan ik weten dat er een patent bestaat?</h2>

<p>Er zijn talloze manieren waarop u op de hoogte kunt raken van het bestaan van een specifiek patent. Behalve dat u rechtstreeks gecontacteerd kunt worden door een patenthouder, kunt u meer te weten komen over een bepaald patent via een zoekopdracht op het internet of via een mailinglijst, of in relatie met uw werk, enz. Als u zich bewust wordt van een patent dat u zorgen baart, kunt u het beste met een advocaat praten in plaats van dergelijke kennis of speculatie te delen op een openbaar forum.</p>

<h2>Wat zijn de verweermiddelen die beschikbaar zijn in een proces wegens patentinbreuk?</h2>

<p>Ten eerste kunnen er veel verweren zijn die specifiek zijn voor de feiten en omstandigheden van een bepaalde situatie, en het is de taak van de advocaat om deze verweren op te sporen en te ontwikkelen. Sommige verweren zijn in de meeste gevallen aanwezig of kunnen dit zijn, en deze omvatten:</p>

<p>Toestemming: U bent niet aansprakelijk voor inbreuk als u toestemming heeft om de conclusies te gebruiken. Een dergelijke toestemming kan expliciet zijn. Een expliciete toestemming wordt een <q>licentie</q> genoemd. Toestemming kan ook impliciet zijn: het kan het gevolg zijn van gedragingen of uitlatingen van de patenthouder die toestemming leken in te houden en waarop u zich hebt gebaseerd. (Advocaten noemen dit <q>uitsluiting</q>.) Ze kan ook het gevolg zijn van regelrechte passiviteit van de octrooihouder, die effectief inbreukmakend gedrag kan toestaan door <q>zijn rechten te laten rusten</q>, wat advocaten <q>nalatigheid</q> noemen.</p>

<p>Geen inbreuk: Een vaststelling van niet-inbreuk is een bewijs dat geen van de patentconclusies daadwerkelijk <q>te lezen is</q> op de ten laste gelegde software. Met andere woorden, de software implementeert niet elk element van wat in een conclusie wordt vermeld.</p>

<p>Ongeldigheid: Als het patent ongeldig of nietig is, kan het niet worden geschonden.  Ongeldigheid kan worden aangetoond door te bewijzen dat het onderwerp van het patent buiten de reikwijdte van het patentrecht valt. Het kan ook worden aangetoond door, volgens de Amerikaanse wetgeving, te staven dat het patent <q>niet nieuw</q> of <q>voor de hand liggend</q> is.  Volgens de patentwetgeving is een patent alleen geldig als de geclaimde uitvinding nuttig, praktisch toepasbaar, nieuw en niet voor de hand liggend is voor een persoon die op het moment dat de uitvinding werd gedaan <q>over de gewone kennis van de techniek</q> beschikte. Een nietigheidsverdediging toont dus aan dat het patent niet aan een van deze vereisten voldoet.</p>

<h1>Het patentrisico voor een gemeenschapsdistributie</h1>

<h2>Kunt u voorbeelden geven van rechtszaken over patentschendingen tegen FOSS-gemeenschappen?</h2>

<p>Nee. Gelukkig zijn er nog maar weinig van dit soort zaken en geen enkele is al tot een vonnis gekomen.  Tot op heden heeft geen enkele rechtbank zich ooit gebogen over de meeste kwesties die uniek zijn voor de distributie van vrije software. Wij geloven dat dit komt omdat FOSS-gemeenschappen geen <q>diepe zakken</q> hebben waaruit ze licentierechten kunnen betalen. En het aanklagen van individuele ontwikkelaars die geen grote inkomsten hebben, zorgt voor slechte pers voor patenthouders zonder dat er een nuttig resultaat wordt bereikt.</p>

<h2>We zijn een FOSS-distributie en we verdienen geen geld. Hoe kunnen we schadevergoeding betalen als deze tegen ons wordt uitgesproken?</h2>

<p>Deze vraag hangt, net als alle andere soortgelijke vragen over de juridische risico's en verantwoordelijkheden van projecten, sterk af van de details van hun juridische structuur en commerciële relaties. Er is geen algemeen antwoord op de vraag hoe projecten omgaan met hun juridische risico's, inclusief de risico's van schadevergoedingsvonnissen voor patentschendingen of andere aansprakelijkheden.
<a href="http://www.softwarefreedom.org">SFLC</a>, de <a href="http://sfconservancy.org/">Software Freedom
Conservancy</a>, de <a href="http://www.apache.org/">Apache Software
Foundation</a>, de <a href="http://www.fsf.org">Free Software
Foundation</a>, <a href="https://www.spi-inc.org/">Software in the Public
Interest</a> en andere organisaties helpen projecten in te passen in juridische contexten en organisaties die deze vragen nuttig en op een algemeen niveau kunnen behandelen. Als uw distributie of een project binnen uw distributie denkt dat het mogelijk juridisch aansprakelijk kan worden gesteld, moet u ons of een van de andere genoemde organisaties raadplegen.</p>

<h2>We zijn een FOSS-distributie en we verdienen geld. Maakt dat ons vatbaarder voor een aanklacht wegens patentschending?</h2>

<p>Iedereen die inkomsten verwerft, is voor een patenthouder een aantrekkelijker doelwit om aan te klagen dan iemand die geen geld verdient waarmee schadevergoeding zou kunnen worden betaald. Een gemeenschapsdistributie die absoluut geen inkomsten heeft, is geen aantrekkelijk doelwit. Maar zelfs als u een paar honderdduizend dollar per jaar omzet haalt uit verkoop, bent u, vergeleken met een winstgevende onderneming ter grootte van Microsoft of zelfs Red Hat, de kosten van een rechtszaak niet waard voor een patenttrol of voor een meer rationele aanklager.</p>

<h2>Ik heb gehoord dat het verspreiden van broncode veiliger is dan het verspreiden van objectcode. Is dat waar?</h2>

<p>Ja. Het verspreiden van broncode is waarschijnlijk veiliger dan het verspreiden van binaire bestanden, om een paar redenen. Ten eerste toont de broncode, net als de patentverklaringen zelf, hoe de uitvinding werkt, in plaats van de uitvinding zelf te zijn. Als broncode op zichzelf inbreuk kan maken op het patent, dan is het moeilijk te begrijpen hoe het uitdelen van fotokopieën van het patent zelf geen inbreuk zou zijn. Ten tweede, in de VS <em>kunnen</em> rechtbanken broncode als spraak beschouwen, wat wij vinden dat ze zouden moeten doen, waardoor broncode onder de bescherming van het Eerste Amendement zou vallen. We weten weinig over hoe het Hooggerechtshof de patentwet in overeenstemming zou brengen met de eisen van het Eerste Amendement.  Wij van SFLC hebben verschillende documenten geschreven voor het Hooggerechtshof over deze kwesties, maar het Hof heeft ze nog nooit behandeld of er een beslissing over genomen. Bovendien kan, zoals hierboven vermeld, aansprakelijkheid voor patentinbreuk worden opgelegd wanneer iemand een ander in staat stelt of aanmoedigt om een patent te schenden, maar de vereisten van kennis en opzet zijn strenger in secundaire aansprakelijkheidssituaties. Omdat een gebruiker eerst de broncode moet compileren en de software moet installeren om inbreuk te maken, zal een rechtbank de gemeenschap minder snel aansprakelijk stellen voor het uitlokken van of bijdragen aan de inbreuk.</p>

<h2>Wie loopt er bij een gemeenschapsdistributieproject de meeste kans om aangeklaagd te worden wegens patentschending?</h2>

<p>Dit is een probleem voor de mogelijke aanklager in een patentzaak, meer dan voor de distributie. Een gemeenschapsdistributie bestaande uit vrijwilligers, zonder enige hiërarchische structuur van tewerkstelling of supervisie, kan niet worden aangeklaagd door <q>het hoofd</q> aan te klagen. Als voor inbreuk opzet en kennis nodig zijn of specifieke inspanningen om het niet te weten, zoals in gevallen van aanzetten tot of bijdragen aan inbreuk, moet deze persoon met deze opzet en kennis waarschijnlijk als individu worden opgespoord en vervolgd. Als mensen die code en documentatie schrijven geen patenten lezen, en de vrijwilligers die code ontwikkelen voor een pakket niet hetzelfde pakket of een gerelateerd pakket onderhouden, kan het voor de aanklager moeilijk zijn om wie dan ook te beschuldigen.</p>

<p>De specifieke kenmerken van een bepaalde situatie zullen echter ongetwijfeld doorslaggevend zijn. Zoals met alle andere soortgelijke zaken moet u onmiddellijk contact opnemen met SFLC of een andere advocaat als u denkt dat het waarschijnlijk is dat er een patent zal worden ingeroepen tegen uw distributie of uw vrijwilligers.</p>

<h2>Suggereert u dat het voor ontwikkelaars en medewerkers beter is om geen patenten te lezen? Zo ja, waarom?</h2>

<p>Ja. Helaas werkt de Amerikaanse patentwetgeving ontmoedigend voor het doorzoeken van patenten, ook al is een van de belangrijkste rechtvaardigingen voor het patentsysteem dat het patent het publiek leert hoe een uitvinding toegepast kan worden die anders geheim zou zijn. Een <q>opzettelijke</q> inbreuk onderwerpt de inbreukmaker aan een hogere schadevergoeding als hij op de hoogte is van het patent en van plan is om inbreuk te plegen, en het lezen van patenten vergroot de kans dat een latere inbreuk opzettelijk zal worden bevonden. Bovendien merken we dat ontwikkelaars er vaak van uitgaan dat de patenten die ze ontdekken, een bredere reikwijdte hebben dan ze in werkelijkheid hebben. Als u desondanks van plan bent om een patentonderzoek uit te voeren, dient u eerst juridisch advies in te winnen.</p>

<h2>Ik bevind me buiten de Verenigde Staten. Is er iets waarover ik me zorgen moet maken?</h2>

<p>Hoewel de meeste landen lid zijn van de Wereldorganisatie voor de Intellectuele Eigendom (World Intellectual Property Organization - WIPO) en het Octrooisamenwerkingsverdrag (Patent Cooperation Treaty - PCT) hebben ondertekend, beperken grote bedrijven hun activiteiten voor het verwerven van patenten over het algemeen tot de <q>Grote Drie</q>: de VS, de EU en Japan. Dit wordt door de meeste bedrijven als een voldoende bescherming beschouwd, hoewel bedrijven steeds vaker patentaanvragen in China indienen in de hoop dat de patentrechten uiteindelijk door de overheid en het bedrijfsleven voldoende zullen worden gerespecteerd. Daarnaast zullen grote multinationale bedrijven in andere rechtsgebieden, zoals Korea en Canada, meestal patentaanvragen indienen in hun eigen land voordat ze internationaal patentaanvragen indienen. In India is sommige software gepatenteerd ondanks de duidelijke wettelijke verklaring dat software <em>op zich</em> niet patenteerbaar is. SFLC is begonnen dergelijke patenten aan te vechten in India.</p>

<p>Maar waar u ook werkt, software die patenten schendt, kan niet worden geïmporteerd in landen waar die patenten zijn verleend, wat betekent dat u zich in ieder geval zorgen moet maken over de mogelijkheid om de beoogde gebruikers te bereiken.</p>

<p>Zoals altijd is overleg met een lokale advocaat een goede stap als u vragen heeft over uw situatie of aansprakelijkheden.</p>

<h2>Zijn er richtlijnen om het risico op patentschending te beperken?</h2>

<p>Ja. Dit document is bedoeld om te informeren over octrooirisico's en hoewel het moeilijk is om advies te geven over de specifieke situatie van elke lezer, zijn er een paar richtlijnen die kunnen worden uitgelicht.</p>

<ul>
<li><p>Het lezen van patenten, vooral wanneer u onderzoekt hoe u uw bijdrage aan uw vrije-softwareproject moet ontwerpen, kan gemeenschappen blootstellen aan aansprakelijkheid die ze anders niet zouden hebben.</p></li>
<li><p>Delen van een vrije-softwaregemeenschap die broncode verspreiden en geen objectcode, lopen waarschijnlijk iets minder patentrisico.</p></li>
<li><p>Vrije software commercieel verspreiden is waarschijnlijk riskanter dan software gratis verspreiden.</p></li>
<li><p>Het vermogen om functies en pakketten snel en eenvoudig uit de distributie te verwijderen, zal helpen om eventuele schade voor de gemeenschap te beperken.</p></li>
<li><p>Procederen over patenten is geen amateursport. Als iemand contact met u opneemt en dreigt u te beschuldigen van een patentinbreuk, neem dan zo snel mogelijk contact op met het Software Freedom Law Center of een andere gekwalificeerde advocaat.</p></li>
</ul>

<hr />

<p><strong>Dankbetuiging.</strong> Dit document werd opgesteld door advocaten van SFLC, met inbreng van Stefano Zacchiroli, namens het Debian Project.</p>
