#use wml::debian::template title="De (wereldwijde) spiegelservers van Debian" MAINPAGE=true
#use wml::debian::translation-check translation="b554c22abdbc4a6253951c9bf9610405d0c4f2cd"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#per-country">Debian-spiegelservers per land</a></li>
  <li><a href="#complete-list">Volledige lijst met spiegelservers</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian wordt gedistribueerd (<a href="https://www.debian.org/mirror/">gespiegeld</a>) op honderden servers. Indien u van plan bent om Debian te <a href="../download">downloaden</a>, probeer dan eerst een nabije server. Dit zal waarschijnlijk sneller gaan en het vermindert ook de belasting op onze centrale servers.<p>
</aside>

<p class="centerblock">
Er bestaan Debian-spiegelservers in verschillende landen en voor sommige
hebben we een alias van de vorm <code>ftp.&lt;land&gt;.debian.org</code>
toegevoegd. Deze alias verwijst naar een spiegelserver die regelmatig en snel
gesynchroniseerd wordt en alle Debian-architecturen bevat. Op de server is het
Debian-archief steeds beschikbaar via HTTP op de locatie
<code>/debian</code>.
</p>

<p class="centerblock">
Voor andere spiegelserversites kunnen restricties gelden in
verband met wat zij spiegelen (door opslagruimtebeperkingen). Een site die niet
de <code>ftp.&lt;land&gt;.debian.org</code> van een land is, is niet per
definitie langzamer of minder bijgewerkt dan de
<code>ftp.&lt;land&gt;.debian.org</code> spiegelserver. Eigenlijk valt een
spiegelserver die uw architectuur bevat en die
zich dicht bij u bevindt en daardoor sneller is, bijna altijd te verkiezen boven
andere verder afgelegen spiegelservers.
</p>

<p>
Gebruik de dichtstbijzijnde site voor de snelst mogelijke downloads—of deze
nu een spiegelserver is met een landspecifieke alias of niet. U kunt het programma
<a href="https://packages.debian.org/stable/net/netselect">\
<code>netselect</code></a> gebruiken om de site met de kortste vertraging te
bepalen; gebruik een downloadprogramma zoals
<a href="https://packages.debian.org/stable/web/wget">\
<code>wget</code></a> of
<a href="https://packages.debian.org/stable/net/rsync">\
<code>rsync</code></a> om te bepalen welke site de hoogste doorvoersnelheid
heeft. Merk op dat de geografische nabijheid vaak niet de belangrijkste factor
is voor het bepalen welke machine u best zal bedienen.
</p>

<p>
Indien u vaak rondtrekt met uw computer, bent u wellicht best
gediend met een "spiegelserver" die ondersteund wordt door een globaal
<abbr title="Content Delivery Network">CDN</abbr> (Content Delivery Network - Netwerk
voor het distribueren van gegevens). Met dit doel onderhoudt het Debian-project
<code>deb.debian.org</code> en u kunt dit gebruiken in uw
<code>sources.list</code>—raadpleeg voor bijkomende informatie de
<a href="http://deb.debian.org/">website</a> van de betrokken dienst.
</p>

<h2  id="per-country">Debian-spiegelservers per land</h2>

<table border="0" class="center">
<tr>
  <th>Land</th>
  <th>Site</th>
  <th>Architecturen</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 id="complete-list">Volledige lijst met spiegelservers</h2>

<table border="0" class="center">
<tr>
  <th>Computernaam</th>
  <th>HTTP</th>
  <th>Architecturen</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
