#use wml::debian::template title="데비안 버그 추적 시스템" BARETITLE=true NOCOPYRIGHT=true
#use wml::debian::translation-check translation="88e6fda5d311846e8cc50c89c3f22aa4a3fa312a" maintainer="Sebul" 
#include "$(ENGLISHDIR)/Bugs/pkgreport-opts.inc"
#{#style#:<link rel="stylesheet" href="https://bugs.debian.org/css/bugs.css" type="text/css">:##}
{#meta#:
<script type="text/javascript" src="hashbug_redirect.js"></script>
:#meta#}

<p>데비안에는 버그 추적 시스템이 있고, 거기에서 사용자 및 개발자가 보고한 버그의 상세를 제공합니다.
각 버그에는 번호가 주어지며, 처리된 것으로 표시될 때까지 파일에 보관됩니다.</p>

<h2>데비안에서 버그 보고하기</h2>

<p>별도의 페이지에는 데비안 배포판에서 버그를 마주쳤을 때
 <a href="Reporting">버그를 보고하는 방법</a>에 대한 설명과 팁이 있습니다.</p>

<h2>버그 추적 시스템 문서</h2>

<ul>
  <li><a href="Developer">시스템 사용 방법에 대한 고급 정보</a></li>
  <li><a href="server-control">메일로 버그를 다루는 것에 대한 정보</a></li>
  <li><a href="server-refcard">메일 서버 참조 카드</a></li>
  <li><a href="Access">버그 보고 로그에 액세스 하는 방법</a></li>
  <li><a href="server-request">메일로 버그 보고 요청</a></li>
</ul>

<h2>WWW에서 버그 보고 보기</h2>

<p style="text-align:center">
<img src="https://qa.debian.org/data/bts/graphs/all.png?m=0.8&amp;h=250&amp;w=600"
alt="Bug count for all" />
</p>

<p><strong>번호</strong>로 버그 찾기:
  <br />
  <a name="bugreport"></a>
  <form method="get" action="https://bugs.debian.org/cgi-bin/bugreport.cgi">
  <p>
  <input type="text" size="9" name="bug" value="">
  <label><input type="checkbox" name="mbox" value="yes"> mbox로</label>
  <label><input type="checkbox" name="trim" value="no"> 모든 헤더 보기</label>
  <label><input type="checkbox" name="boring" value="yes"> 지루한 메시지 보기</label>
  <input type="submit" value="찾기">
  </p>
  </form>

<h2>WWW에서 버그 보고 선택</h2>
<a name="pkgreport"></a>

<bts_main_form>

<table class="forms">

<tr><td><h2>버그 선택</h2>
</td>
<td>
<bts_select_form>
</td>
<td>
<p>첫 번째 검색 후에 더 많은 선택 항목을 추가할 수 있습니다. 나중 선택 항목이 동일한 검색 필드에 있으면 결과가 OR됩니다. 다른 필드에 있으면 결과가 AND됩니다.
</p>
<p>유효한 심각도 <bts_severities_all>.</p>
<p>유효한 태그 <bts_tags>.</p>
</td>
</tr>

<tr><td><h2>버그 포함</h2></td>
<td>
<bts_include_form>
</td>
<td>
</td>
</tr>

<tr><td><h2>버그 제외</h2></td>
<td>
<bts_exclude_form>
</td>
<td></td>
</tr>

<tr><td><h2>정렬 기준</h2></td>
<td>
<bts_orderby_form>
</td>
<td></td>
</tr>

<tr><td><h2>여러 옵션</h2></td>
<td>
<bts_miscopts_form>
</td>
</tr>

<tr><td><h2>제출</h2></td><td colspan=2>
<input type="submit" name="submit" value="제출">
</td></tr>

</table>
</form>

<p>위의 쿼리는 각각 다음 형식의 URL을 방문하여 만들 수도 있습니다:</p>
<ul>
  <li><tt>https://bugs.debian.org/<var>number</var></tt></li>
  <li><tt>https://bugs.debian.org/mbox:<var>number</var></tt></li>
  <li><tt>https://bugs.debian.org/<var>package</var></tt></li>
  <li><tt>https://bugs.debian.org/src:<var>sourcepackage</var></tt></li>
  <li><tt>https://bugs.debian.org/<var>maintainer@email.address</var></tt></li>
  <li><tt>https://bugs.debian.org/from:<var>submitter@email.address</var></tt></li>
  <li><tt>https://bugs.debian.org/severity:<var>severity</var></tt></li>
  <li><tt>https://bugs.debian.org/tag:<var>tag</var></tt></li>
</ul>

<h2>버그 보고 검색</h2>

<p><a href="https://bugs.debian.org/cgi-bin/search.cgi">HyperEstraier
based search engine</a>을 써서 버그 보고를 검색할 수 있습니다.</p>

<p>버그 보고를 검색하는 다른 방법 
<a href="https://groups.google.com/d/forum/linux.debian.bugs.dist">Google Groups</a>.
The period to be searched can be limited by using the
<a href="https://groups.google.com/d/search/group%3Alinux.debian.bugs.dist">\
advanced search</a> option.</p>

<p>버그 보고를 검색할 수 있는 대체 사이트 
<a href="https://www.mail-archive.com/debian-bugs-dist%40lists.debian.org/">The
Mail Archive</a>.</p>

<h2>추가 정보</h2>

<p><a href="https://bugs.debian.org/release-critical/">
Release Critical Bugs</a>의 현재 목록.</p>

<p>데비안 버그 추적 시스템에서 인식하는 <a href="pseudo-packages">pseudo-packages</a>
목록.</p>

<p>다음 버그 보고서 인덱스 가능:</p>

<ul>
  <li><a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=pkg">active</a>
      및
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=pkg&amp;archive=yes">archived</a>
      버고 보고 있는 페이지.</li>
  <li>
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=src">active</a>
      및
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=src&amp;archive=yes">archived</a>
      버고 보고가 있는 소스 패키지.</li>
  <li>
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=maint">active</a>
      및
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=maint&amp;archive=yes">archived</a>
      버그 보고가 있는 패키지 관리자.</li>
  <li>
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=submitter">active</a>
      및
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=submitter&amp;archive=yes">archived</a>
      버그 보고 제출자.</li>
</ul>

<p><strong>주의:</strong> 
이전에 사용 가능했던 버그 보고서 인덱스 중 일부는 버그 보고서를 생성한 프로그램의 내부 문제로 인해 사용할 수 없습니다. 
불편을 끼쳐드려 죄송합니다.</p>

<h2>스팸 보고</h2>
<p>
버그 추적 시스템은 종종 스팸을 받습니다. 
버그 추적 시스템에서 스팸을 보고하려면 <a href="#bugreport">번호별</a>로 
버그 보고서를 찾고, 하단 근처에서 "this bug log contains spam"을 클릭하세요.
</p>

#include "$(ENGLISHDIR)/Bugs/footer.inc"
